EduSat Drivers
# USTTL EduSat

EduSat System software and Ground Station.
Includes OBC (STM32F407) drivers and subsystem codes for Bluepill (STM32F103C).

**Let's Get Started**
1.  Development Environment Setup
    *  Download and Install Keil uVision IDE from the [official source](https://www.keil.com/demo/eval/arm.htm).
       You may be required to provide additional information.
    * Click "Pack Installer"
    * Select the desired MCU (STM32F407VETx) and press Install Package


2. Writing Up Code
    * Download the EduSat Software repository
    * Start Keil. Select Project -> Open
    * Navigate to MDK-ARM directory and select the ".uvoprojx" file
    * Navigate to main.c to start coding.
    * After finishing code, press F7 to Build.


3. Downloading Code to EduSat
    * Connect ST-Link V2 to OBC
    * Press Ctrl + F5 to enter Debug mode
    * Press F5 to load the code on the flash of OBC
    * (Optional) Choose watch window to observe the variables as they change