﻿namespace EduSatGS
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.tmrPhototimeout = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txbBaud = new System.Windows.Forms.TextBox();
            this.btnBaglan = new System.Windows.Forms.Button();
            this.btnPortYenile = new System.Windows.Forms.Button();
            this.lblBaglanti = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbAcikPortlar = new System.Windows.Forms.ComboBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbTelemetryStatus = new System.Windows.Forms.ListBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusGunZaman = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrZaman = new System.Windows.Forms.Timer(this.components);
            this.btnSavePhoto = new System.Windows.Forms.Button();
            this.btnGetPhoto = new System.Windows.Forms.Button();
            this.btnClearImage = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button15 = new System.Windows.Forms.Button();
            this.resbox = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.button14 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.periodbox = new System.Windows.Forms.GroupBox();
            this.periodB = new System.Windows.Forms.TextBox();
            this.button13 = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.GPSIndicator = new System.Windows.Forms.PictureBox();
            this.telemetryIndicator = new System.Windows.Forms.PictureBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.OpenFileB = new System.Windows.Forms.Button();
            this.SaveB = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.AntennaIndicator = new System.Windows.Forms.PictureBox();
            this.label26 = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.ms2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textProgressBar1 = new EduSatGS.TextProgressBar();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.periodbox.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GPSIndicator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telemetryIndicator)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AntennaIndicator)).BeginInit();
            this.groupBox15.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // serialPort1
            // 
            this.serialPort1.DtrEnable = true;
            this.serialPort1.ReadBufferSize = 230000;
            this.serialPort1.RtsEnable = true;
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.SerialPort1_DataReceived);
            // 
            // tmrPhototimeout
            // 
            this.tmrPhototimeout.Interval = 1000;
            this.tmrPhototimeout.Tick += new System.EventHandler(this.TmrPhototimeout_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txbBaud);
            this.groupBox1.Controls.Add(this.btnBaglan);
            this.groupBox1.Controls.Add(this.btnPortYenile);
            this.groupBox1.Controls.Add(this.lblBaglanti);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbAcikPortlar);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(303, 86);
            this.groupBox1.TabIndex = 128;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Serial Port Connection";
            // 
            // txbBaud
            // 
            this.txbBaud.Location = new System.Drawing.Point(47, 52);
            this.txbBaud.Name = "txbBaud";
            this.txbBaud.Size = new System.Drawing.Size(85, 20);
            this.txbBaud.TabIndex = 6;
            this.txbBaud.Text = "9600";
            this.txbBaud.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnBaglan
            // 
            this.btnBaglan.Location = new System.Drawing.Point(219, 21);
            this.btnBaglan.Name = "btnBaglan";
            this.btnBaglan.Size = new System.Drawing.Size(75, 51);
            this.btnBaglan.TabIndex = 5;
            this.btnBaglan.Text = "Connect";
            this.btnBaglan.UseVisualStyleBackColor = true;
            this.btnBaglan.Click += new System.EventHandler(this.BtnBaglan_Click);
            // 
            // btnPortYenile
            // 
            this.btnPortYenile.Location = new System.Drawing.Point(138, 21);
            this.btnPortYenile.Name = "btnPortYenile";
            this.btnPortYenile.Size = new System.Drawing.Size(75, 23);
            this.btnPortYenile.TabIndex = 4;
            this.btnPortYenile.Text = "Refresh Ports";
            this.btnPortYenile.UseVisualStyleBackColor = true;
            this.btnPortYenile.Click += new System.EventHandler(this.BtnPortYenile_Click);
            // 
            // lblBaglanti
            // 
            this.lblBaglanti.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBaglanti.Location = new System.Drawing.Point(144, 55);
            this.lblBaglanti.Name = "lblBaglanti";
            this.lblBaglanti.Size = new System.Drawing.Size(67, 13);
            this.lblBaglanti.TabIndex = 3;
            this.lblBaglanti.Text = "No Connection";
            this.lblBaglanti.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Baud:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Port:";
            // 
            // cbAcikPortlar
            // 
            this.cbAcikPortlar.FormattingEnabled = true;
            this.cbAcikPortlar.Location = new System.Drawing.Point(47, 23);
            this.cbAcikPortlar.Name = "cbAcikPortlar";
            this.cbAcikPortlar.Size = new System.Drawing.Size(85, 21);
            this.cbAcikPortlar.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbTelemetryStatus);
            this.groupBox3.Location = new System.Drawing.Point(321, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(342, 86);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Telemetry Status";
            // 
            // lbTelemetryStatus
            // 
            this.lbTelemetryStatus.FormattingEnabled = true;
            this.lbTelemetryStatus.Location = new System.Drawing.Point(6, 19);
            this.lbTelemetryStatus.Name = "lbTelemetryStatus";
            this.lbTelemetryStatus.Size = new System.Drawing.Size(330, 56);
            this.lbTelemetryStatus.TabIndex = 0;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox2.Image = global::EduSatGS.Properties.Resources.Logo1;
            this.pictureBox2.Location = new System.Drawing.Point(0, 389);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(146, 140);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusGunZaman});
            this.statusStrip1.Location = new System.Drawing.Point(527, 648);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(137, 22);
            this.statusStrip1.TabIndex = 130;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusGunZaman
            // 
            this.toolStripStatusGunZaman.Name = "toolStripStatusGunZaman";
            this.toolStripStatusGunZaman.Size = new System.Drawing.Size(120, 17);
            this.toolStripStatusGunZaman.Text = "6 Kasım 2019 20:17:13";
            // 
            // tmrZaman
            // 
            this.tmrZaman.Interval = 1000;
            this.tmrZaman.Tick += new System.EventHandler(this.TmrZaman_Tick);
            // 
            // btnSavePhoto
            // 
            this.btnSavePhoto.Enabled = false;
            this.btnSavePhoto.Location = new System.Drawing.Point(90, 12);
            this.btnSavePhoto.Name = "btnSavePhoto";
            this.btnSavePhoto.Size = new System.Drawing.Size(75, 23);
            this.btnSavePhoto.TabIndex = 12;
            this.btnSavePhoto.Text = "Save Photo";
            this.btnSavePhoto.UseVisualStyleBackColor = true;
            this.btnSavePhoto.Click += new System.EventHandler(this.BtnSavePhoto_Click);
            // 
            // btnGetPhoto
            // 
            this.btnGetPhoto.Enabled = false;
            this.btnGetPhoto.Location = new System.Drawing.Point(9, 12);
            this.btnGetPhoto.Name = "btnGetPhoto";
            this.btnGetPhoto.Size = new System.Drawing.Size(75, 23);
            this.btnGetPhoto.TabIndex = 1;
            this.btnGetPhoto.Text = "Get Photo";
            this.btnGetPhoto.UseVisualStyleBackColor = true;
            this.btnGetPhoto.Click += new System.EventHandler(this.BtnGetPhoto_Click);
            // 
            // btnClearImage
            // 
            this.btnClearImage.Enabled = false;
            this.btnClearImage.Location = new System.Drawing.Point(171, 12);
            this.btnClearImage.Name = "btnClearImage";
            this.btnClearImage.Size = new System.Drawing.Size(75, 23);
            this.btnClearImage.TabIndex = 13;
            this.btnClearImage.Text = "Clear Photo";
            this.btnClearImage.UseVisualStyleBackColor = true;
            this.btnClearImage.Click += new System.EventHandler(this.BtnClearImage_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button15);
            this.groupBox2.Controls.Add(this.resbox);
            this.groupBox2.Controls.Add(this.textProgressBar1);
            this.groupBox2.Controls.Add(this.btnClearImage);
            this.groupBox2.Controls.Add(this.btnGetPhoto);
            this.groupBox2.Controls.Add(this.btnSavePhoto);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Location = new System.Drawing.Point(-4, -9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(650, 554);
            this.groupBox2.TabIndex = 129;
            this.groupBox2.TabStop = false;
            // 
            // button15
            // 
            this.button15.Enabled = false;
            this.button15.Location = new System.Drawing.Point(600, 14);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(40, 21);
            this.button15.TabIndex = 135;
            this.button15.Text = "Set";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // resbox
            // 
            this.resbox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.resbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resbox.FormattingEnabled = true;
            this.resbox.Items.AddRange(new object[] {
            "Resolution",
            "   320 x 240",
            "   640 x 480",
            "  1024 x 768",
            "  1280 x 960",
            " 1600 x 1200",
            " 2048 x 1536",
            " 2592 x 1944"});
            this.resbox.Location = new System.Drawing.Point(519, 14);
            this.resbox.Name = "resbox";
            this.resbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.resbox.Size = new System.Drawing.Size(75, 21);
            this.resbox.TabIndex = 134;
            this.resbox.DropDown += new System.EventHandler(this.resbox_DropDown);
            this.resbox.DropDownClosed += new System.EventHandler(this.resbox_DropDownClosed);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(7, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(635, 491);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 104);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(651, 547);
            this.tabControl1.TabIndex = 131;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage3.Controls.Add(this.textBox5);
            this.tabPage3.Controls.Add(this.pictureBox4);
            this.tabPage3.Controls.Add(this.pictureBox3);
            this.tabPage3.Controls.Add(this.pictureBox2);
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Controls.Add(this.periodbox);
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.groupBox6);
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage3.Size = new System.Drawing.Size(643, 521);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Commands";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(2, 397);
            this.textBox5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(144, 19);
            this.textBox5.TabIndex = 20;
            this.textBox5.Text = "Made in Turkey by";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox4.Image = global::EduSatGS.Properties.Resources.logo_nanosat;
            this.pictureBox4.Location = new System.Drawing.Point(489, 378);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(152, 146);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 19;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox3.Image = global::EduSatGS.Properties.Resources.CNRS_logo;
            this.pictureBox3.Location = new System.Drawing.Point(274, 436);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(210, 90);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 18;
            this.pictureBox3.TabStop = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.button14);
            this.groupBox8.Controls.Add(this.button12);
            this.groupBox8.Location = new System.Drawing.Point(305, 212);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(234, 55);
            this.groupBox8.TabIndex = 15;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "System";
            // 
            // button14
            // 
            this.button14.Enabled = false;
            this.button14.Location = new System.Drawing.Point(118, 25);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(110, 23);
            this.button14.TabIndex = 11;
            this.button14.Text = "Open Antennas";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button12
            // 
            this.button12.Enabled = false;
            this.button12.Location = new System.Drawing.Point(18, 25);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(82, 23);
            this.button12.TabIndex = 10;
            this.button12.Text = "Soft Reset";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // periodbox
            // 
            this.periodbox.Controls.Add(this.periodB);
            this.periodbox.Controls.Add(this.button13);
            this.periodbox.Location = new System.Drawing.Point(305, 151);
            this.periodbox.Name = "periodbox";
            this.periodbox.Size = new System.Drawing.Size(234, 55);
            this.periodbox.TabIndex = 14;
            this.periodbox.TabStop = false;
            this.periodbox.Text = "Period";
            // 
            // periodB
            // 
            this.periodB.Location = new System.Drawing.Point(6, 25);
            this.periodB.Name = "periodB";
            this.periodB.Size = new System.Drawing.Size(134, 20);
            this.periodB.TabIndex = 11;
            this.periodB.TextChanged += new System.EventHandler(this.periodB_TextChanged);
            this.periodB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.periodB_KeyDown);
            this.periodB.KeyUp += new System.Windows.Forms.KeyEventHandler(this.periodB_KeyUp);
            // 
            // button13
            // 
            this.button13.Enabled = false;
            this.button13.Location = new System.Drawing.Point(146, 23);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(82, 23);
            this.button13.TabIndex = 10;
            this.button13.Text = "Set Period";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Controls.Add(this.textBox3);
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Controls.Add(this.button11);
            this.groupBox7.Controls.Add(this.button10);
            this.groupBox7.Controls.Add(this.button9);
            this.groupBox7.Controls.Add(this.label4);
            this.groupBox7.Controls.Add(this.radioButton2);
            this.groupBox7.Controls.Add(this.radioButton1);
            this.groupBox7.Controls.Add(this.textBox2);
            this.groupBox7.Controls.Add(this.button8);
            this.groupBox7.Location = new System.Drawing.Point(43, 151);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(217, 147);
            this.groupBox7.TabIndex = 13;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Reaction Wheel";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(169, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 16);
            this.label6.TabIndex = 18;
            this.label6.Text = "rpm";
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(109, 120);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(58, 20);
            this.textBox3.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 16);
            this.label5.TabIndex = 17;
            this.label5.Text = "Current Speed:";
            // 
            // button11
            // 
            this.button11.Enabled = false;
            this.button11.Location = new System.Drawing.Point(147, 86);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(64, 23);
            this.button11.TabIndex = 17;
            this.button11.Text = "Reverse!";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Enabled = false;
            this.button10.Location = new System.Drawing.Point(80, 86);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(61, 23);
            this.button10.TabIndex = 16;
            this.button10.Text = "Stop!";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.Enabled = false;
            this.button9.Location = new System.Drawing.Point(6, 86);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(68, 23);
            this.button9.TabIndex = 14;
            this.button9.Text = "Spin!";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 16);
            this.label4.TabIndex = 14;
            this.label4.Text = "Direction:";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(144, 59);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(50, 17);
            this.radioButton2.TabIndex = 15;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "CCW";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(91, 59);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(43, 17);
            this.radioButton1.TabIndex = 14;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "CW";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(6, 21);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(134, 20);
            this.textBox2.TabIndex = 14;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.textBox2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox2_KeyUp);
            // 
            // button8
            // 
            this.button8.Enabled = false;
            this.button8.Location = new System.Drawing.Point(146, 19);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(65, 23);
            this.button8.TabIndex = 9;
            this.button8.Text = "Set Speed";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBox1);
            this.groupBox6.Controls.Add(this.button6);
            this.groupBox6.Controls.Add(this.button7);
            this.groupBox6.Location = new System.Drawing.Point(305, 23);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(234, 91);
            this.groupBox6.TabIndex = 12;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Buzzer";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 25);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(134, 20);
            this.textBox1.TabIndex = 11;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // button6
            // 
            this.button6.Enabled = false;
            this.button6.Location = new System.Drawing.Point(89, 57);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(66, 23);
            this.button6.TabIndex = 9;
            this.button6.Text = "Buzz!";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Enabled = false;
            this.button7.Location = new System.Drawing.Point(146, 22);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(82, 23);
            this.button7.TabIndex = 10;
            this.button7.Text = "Set Message";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.GPSIndicator);
            this.groupBox5.Controls.Add(this.telemetryIndicator);
            this.groupBox5.Controls.Add(this.button4);
            this.groupBox5.Controls.Add(this.button5);
            this.groupBox5.Location = new System.Drawing.Point(43, 23);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 91);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Telemetry";
            // 
            // GPSIndicator
            // 
            this.GPSIndicator.Image = global::EduSatGS.Properties.Resources.R;
            this.GPSIndicator.Location = new System.Drawing.Point(13, 57);
            this.GPSIndicator.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.GPSIndicator.Name = "GPSIndicator";
            this.GPSIndicator.Size = new System.Drawing.Size(24, 23);
            this.GPSIndicator.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.GPSIndicator.TabIndex = 12;
            this.GPSIndicator.TabStop = false;
            this.GPSIndicator.Visible = false;
            // 
            // telemetryIndicator
            // 
            this.telemetryIndicator.Image = global::EduSatGS.Properties.Resources.R;
            this.telemetryIndicator.Location = new System.Drawing.Point(13, 19);
            this.telemetryIndicator.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.telemetryIndicator.Name = "telemetryIndicator";
            this.telemetryIndicator.Size = new System.Drawing.Size(24, 23);
            this.telemetryIndicator.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.telemetryIndicator.TabIndex = 11;
            this.telemetryIndicator.TabStop = false;
            this.telemetryIndicator.Visible = false;
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Location = new System.Drawing.Point(41, 19);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(131, 23);
            this.button4.TabIndex = 9;
            this.button4.Text = "Get Telemetry";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button2_Click);
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(41, 57);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(131, 23);
            this.button5.TabIndex = 10;
            this.button5.Text = "Get GPS Only";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.Controls.Add(this.OpenFileB);
            this.tabPage2.Controls.Add(this.SaveB);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox23);
            this.tabPage2.Controls.Add(this.groupBox16);
            this.tabPage2.Controls.Add(this.groupBox14);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage2.Size = new System.Drawing.Size(643, 521);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Telemetry";
            // 
            // OpenFileB
            // 
            this.OpenFileB.Enabled = false;
            this.OpenFileB.Location = new System.Drawing.Point(134, 475);
            this.OpenFileB.Name = "OpenFileB";
            this.OpenFileB.Size = new System.Drawing.Size(117, 23);
            this.OpenFileB.TabIndex = 15;
            this.OpenFileB.Text = "Open File";
            this.OpenFileB.UseVisualStyleBackColor = true;
            this.OpenFileB.Click += new System.EventHandler(this.OpenFileB_Click);
            // 
            // SaveB
            // 
            this.SaveB.Enabled = false;
            this.SaveB.Location = new System.Drawing.Point(11, 475);
            this.SaveB.Name = "SaveB";
            this.SaveB.Size = new System.Drawing.Size(117, 23);
            this.SaveB.TabIndex = 14;
            this.SaveB.Text = "Save to File";
            this.SaveB.UseVisualStyleBackColor = true;
            this.SaveB.Click += new System.EventHandler(this.SaveB_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.checkedListBox1);
            this.groupBox4.Location = new System.Drawing.Point(402, 314);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(187, 192);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Health Status";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 171);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "*Check indicates an error";
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.checkedListBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBox1.Enabled = false;
            this.checkedListBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "Accel-Gyro",
            "Magnetometer",
            "Barometer",
            "SD Card",
            "Camera",
            "Reaction Wheel",
            "EPS",
            "ADCS"});
            this.checkedListBox1.Location = new System.Drawing.Point(6, 19);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkedListBox1.Size = new System.Drawing.Size(175, 136);
            this.checkedListBox1.TabIndex = 0;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.groupBox25);
            this.groupBox23.Controls.Add(this.groupBox24);
            this.groupBox23.Location = new System.Drawing.Point(351, 193);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox23.Size = new System.Drawing.Size(285, 99);
            this.groupBox23.TabIndex = 12;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "ADCS";
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.label39);
            this.groupBox25.Controls.Add(this.textBox43);
            this.groupBox25.Location = new System.Drawing.Point(155, 21);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox25.Size = new System.Drawing.Size(119, 49);
            this.groupBox25.TabIndex = 6;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "RW Speed";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(89, 22);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(24, 13);
            this.label39.TabIndex = 10;
            this.label39.Text = "rpm";
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(16, 20);
            this.textBox43.Name = "textBox43";
            this.textBox43.ReadOnly = true;
            this.textBox43.Size = new System.Drawing.Size(67, 20);
            this.textBox43.TabIndex = 1;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.textBox39);
            this.groupBox24.Controls.Add(this.textBox36);
            this.groupBox24.Controls.Add(this.textBox37);
            this.groupBox24.Controls.Add(this.textBox38);
            this.groupBox24.Location = new System.Drawing.Point(15, 16);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox24.Size = new System.Drawing.Size(117, 77);
            this.groupBox24.TabIndex = 0;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Light Intensity";
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(58, 45);
            this.textBox39.Name = "textBox39";
            this.textBox39.ReadOnly = true;
            this.textBox39.Size = new System.Drawing.Size(44, 20);
            this.textBox39.TabIndex = 5;
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(6, 45);
            this.textBox36.Name = "textBox36";
            this.textBox36.ReadOnly = true;
            this.textBox36.Size = new System.Drawing.Size(44, 20);
            this.textBox36.TabIndex = 4;
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(58, 19);
            this.textBox37.Name = "textBox37";
            this.textBox37.ReadOnly = true;
            this.textBox37.Size = new System.Drawing.Size(44, 20);
            this.textBox37.TabIndex = 3;
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(6, 19);
            this.textBox38.Name = "textBox38";
            this.textBox38.ReadOnly = true;
            this.textBox38.Size = new System.Drawing.Size(44, 20);
            this.textBox38.TabIndex = 1;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.groupBox18);
            this.groupBox16.Controls.Add(this.groupBox20);
            this.groupBox16.Location = new System.Drawing.Point(11, 314);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(371, 124);
            this.groupBox16.TabIndex = 11;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "GPS";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label29);
            this.groupBox18.Controls.Add(this.textBox27);
            this.groupBox18.Controls.Add(this.label30);
            this.groupBox18.Controls.Add(this.textBox28);
            this.groupBox18.Location = new System.Drawing.Point(6, 19);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(359, 44);
            this.groupBox18.TabIndex = 0;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Date";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(182, 22);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(33, 13);
            this.label29.TabIndex = 4;
            this.label29.Text = "Date:";
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(221, 18);
            this.textBox27.Name = "textBox27";
            this.textBox27.ReadOnly = true;
            this.textBox27.Size = new System.Drawing.Size(113, 20);
            this.textBox27.TabIndex = 3;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 22);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(33, 13);
            this.label30.TabIndex = 2;
            this.label30.Text = "Time:";
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(39, 19);
            this.textBox28.Name = "textBox28";
            this.textBox28.ReadOnly = true;
            this.textBox28.Size = new System.Drawing.Size(117, 20);
            this.textBox28.TabIndex = 1;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.label43);
            this.groupBox20.Controls.Add(this.label42);
            this.groupBox20.Controls.Add(this.label41);
            this.groupBox20.Controls.Add(this.label33);
            this.groupBox20.Controls.Add(this.textBox31);
            this.groupBox20.Controls.Add(this.label34);
            this.groupBox20.Controls.Add(this.textBox32);
            this.groupBox20.Controls.Add(this.label35);
            this.groupBox20.Controls.Add(this.textBox33);
            this.groupBox20.Location = new System.Drawing.Point(6, 69);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(359, 44);
            this.groupBox20.TabIndex = 1;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Coordinates";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(337, 22);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(15, 13);
            this.label43.TabIndex = 11;
            this.label43.Text = "m";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(90, 22);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(11, 13);
            this.label42.TabIndex = 16;
            this.label42.Text = "°";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(193, 22);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(11, 13);
            this.label41.TabIndex = 9;
            this.label41.Text = "°";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(204, 22);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(45, 13);
            this.label33.TabIndex = 4;
            this.label33.Text = "Altitude:";
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(255, 19);
            this.textBox31.Name = "textBox31";
            this.textBox31.ReadOnly = true;
            this.textBox31.Size = new System.Drawing.Size(79, 20);
            this.textBox31.TabIndex = 3;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(107, 21);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(28, 13);
            this.label34.TabIndex = 4;
            this.label34.Text = "Lon:";
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(141, 19);
            this.textBox32.Name = "textBox32";
            this.textBox32.ReadOnly = true;
            this.textBox32.Size = new System.Drawing.Size(49, 20);
            this.textBox32.TabIndex = 3;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 22);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(25, 13);
            this.label35.TabIndex = 2;
            this.label35.Text = "Lat:";
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(37, 18);
            this.textBox33.Name = "textBox33";
            this.textBox33.ReadOnly = true;
            this.textBox33.Size = new System.Drawing.Size(49, 20);
            this.textBox33.TabIndex = 1;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.AntennaIndicator);
            this.groupBox14.Controls.Add(this.label26);
            this.groupBox14.Controls.Add(this.groupBox15);
            this.groupBox14.Controls.Add(this.groupBox17);
            this.groupBox14.Location = new System.Drawing.Point(351, 44);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(285, 143);
            this.groupBox14.TabIndex = 10;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "EPS";
            // 
            // AntennaIndicator
            // 
            this.AntennaIndicator.Image = global::EduSatGS.Properties.Resources.R;
            this.AntennaIndicator.Location = new System.Drawing.Point(126, 118);
            this.AntennaIndicator.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.AntennaIndicator.Name = "AntennaIndicator";
            this.AntennaIndicator.Size = new System.Drawing.Size(21, 21);
            this.AntennaIndicator.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.AntennaIndicator.TabIndex = 12;
            this.AntennaIndicator.TabStop = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(44, 122);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(83, 13);
            this.label26.TabIndex = 9;
            this.label26.Text = "Antenna Status:";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label36);
            this.groupBox15.Controls.Add(this.label32);
            this.groupBox15.Controls.Add(this.label23);
            this.groupBox15.Controls.Add(this.textBox21);
            this.groupBox15.Controls.Add(this.label25);
            this.groupBox15.Controls.Add(this.textBox23);
            this.groupBox15.Location = new System.Drawing.Point(6, 19);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(273, 44);
            this.groupBox15.TabIndex = 0;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Battery";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(246, 21);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(22, 13);
            this.label36.TabIndex = 7;
            this.label36.Text = "mA";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(112, 22);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(14, 13);
            this.label32.TabIndex = 6;
            this.label32.Text = "V";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(133, 21);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(44, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Current:";
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(183, 18);
            this.textBox21.Name = "textBox21";
            this.textBox21.ReadOnly = true;
            this.textBox21.Size = new System.Drawing.Size(57, 20);
            this.textBox21.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(46, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "Voltage:";
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(58, 18);
            this.textBox23.Name = "textBox23";
            this.textBox23.ReadOnly = true;
            this.textBox23.Size = new System.Drawing.Size(52, 20);
            this.textBox23.TabIndex = 1;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.label38);
            this.groupBox17.Controls.Add(this.label37);
            this.groupBox17.Controls.Add(this.label24);
            this.groupBox17.Controls.Add(this.textBox26);
            this.groupBox17.Controls.Add(this.textBox22);
            this.groupBox17.Controls.Add(this.label28);
            this.groupBox17.Location = new System.Drawing.Point(6, 69);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(273, 44);
            this.groupBox17.TabIndex = 1;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Solar Panels";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(112, 22);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(14, 13);
            this.label38.TabIndex = 10;
            this.label38.Text = "V";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(246, 21);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(22, 13);
            this.label37.TabIndex = 9;
            this.label37.Text = "mA";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(135, 21);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(44, 13);
            this.label24.TabIndex = 8;
            this.label24.Text = "Current:";
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(58, 18);
            this.textBox26.Name = "textBox26";
            this.textBox26.ReadOnly = true;
            this.textBox26.Size = new System.Drawing.Size(52, 20);
            this.textBox26.TabIndex = 5;
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(185, 18);
            this.textBox22.Name = "textBox22";
            this.textBox22.ReadOnly = true;
            this.textBox22.Size = new System.Drawing.Size(57, 20);
            this.textBox22.TabIndex = 7;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(46, 13);
            this.label28.TabIndex = 6;
            this.label28.Text = "Voltage:";
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(335, 5);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "Clear";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(242, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Update";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.groupBox10);
            this.groupBox9.Controls.Add(this.groupBox11);
            this.groupBox9.Controls.Add(this.groupBox12);
            this.groupBox9.Controls.Add(this.groupBox13);
            this.groupBox9.Location = new System.Drawing.Point(11, 45);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(294, 247);
            this.groupBox9.TabIndex = 7;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Sensors";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label31);
            this.groupBox10.Controls.Add(this.ms2);
            this.groupBox10.Controls.Add(this.label11);
            this.groupBox10.Controls.Add(this.textBox9);
            this.groupBox10.Controls.Add(this.label13);
            this.groupBox10.Controls.Add(this.textBox11);
            this.groupBox10.Controls.Add(this.label14);
            this.groupBox10.Controls.Add(this.textBox12);
            this.groupBox10.Location = new System.Drawing.Point(6, 19);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(282, 44);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Accelerometer";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(238, 22);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(28, 13);
            this.label31.TabIndex = 6;
            this.label31.Text = "m/s²";
            // 
            // ms2
            // 
            this.ms2.AutoSize = true;
            this.ms2.Location = new System.Drawing.Point(245, 22);
            this.ms2.Name = "ms2";
            this.ms2.Size = new System.Drawing.Size(0, 13);
            this.ms2.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(162, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Z:";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(185, 18);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(49, 20);
            this.textBox9.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(84, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Y:";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(107, 18);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(49, 20);
            this.textBox11.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "X:";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(29, 18);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(49, 20);
            this.textBox12.TabIndex = 1;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label27);
            this.groupBox11.Controls.Add(this.label12);
            this.groupBox11.Controls.Add(this.label10);
            this.groupBox11.Controls.Add(this.textBox4);
            this.groupBox11.Controls.Add(this.label7);
            this.groupBox11.Controls.Add(this.label15);
            this.groupBox11.Controls.Add(this.textBox13);
            this.groupBox11.Controls.Add(this.label16);
            this.groupBox11.Controls.Add(this.textBox14);
            this.groupBox11.Location = new System.Drawing.Point(6, 169);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(282, 72);
            this.groupBox11.TabIndex = 6;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Barometer";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(182, 49);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(15, 13);
            this.label27.TabIndex = 10;
            this.label27.Text = "m";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(252, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(20, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Pa";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(94, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(18, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "°C";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(121, 46);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(55, 20);
            this.textBox4.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(78, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Altitude:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(118, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Pressure:";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(175, 18);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(71, 20);
            this.textBox13.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Temp:";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(49, 18);
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.Size = new System.Drawing.Size(39, 20);
            this.textBox14.TabIndex = 1;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label8);
            this.groupBox12.Controls.Add(this.label17);
            this.groupBox12.Controls.Add(this.textBox15);
            this.groupBox12.Controls.Add(this.label18);
            this.groupBox12.Controls.Add(this.textBox16);
            this.groupBox12.Controls.Add(this.label19);
            this.groupBox12.Controls.Add(this.textBox17);
            this.groupBox12.Location = new System.Drawing.Point(6, 69);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(282, 44);
            this.groupBox12.TabIndex = 1;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Gyroscope";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(240, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "deg/s";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(162, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Z:";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(185, 18);
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(49, 20);
            this.textBox15.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(84, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "Y:";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(107, 18);
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.Size = new System.Drawing.Size(49, 20);
            this.textBox16.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "X:";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(29, 18);
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.Size = new System.Drawing.Size(49, 20);
            this.textBox17.TabIndex = 1;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label9);
            this.groupBox13.Controls.Add(this.label20);
            this.groupBox13.Controls.Add(this.textBox18);
            this.groupBox13.Controls.Add(this.label21);
            this.groupBox13.Controls.Add(this.textBox19);
            this.groupBox13.Controls.Add(this.label22);
            this.groupBox13.Controls.Add(this.textBox20);
            this.groupBox13.Location = new System.Drawing.Point(6, 119);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(282, 44);
            this.groupBox13.TabIndex = 5;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Magnetometer";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(238, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "deg";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(162, 21);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(17, 13);
            this.label20.TabIndex = 4;
            this.label20.Text = "Z:";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(185, 18);
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(49, 20);
            this.textBox18.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(84, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "Y:";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(107, 18);
            this.textBox19.Name = "textBox19";
            this.textBox19.ReadOnly = true;
            this.textBox19.Size = new System.Drawing.Size(49, 20);
            this.textBox19.TabIndex = 3;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(17, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "X:";
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(29, 18);
            this.textBox20.Name = "textBox20";
            this.textBox20.ReadOnly = true;
            this.textBox20.Size = new System.Drawing.Size(49, 20);
            this.textBox20.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Size = new System.Drawing.Size(643, 521);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Photo";
            // 
            // textProgressBar1
            // 
            this.textProgressBar1.CustomText = "";
            this.textProgressBar1.Location = new System.Drawing.Point(252, 12);
            this.textProgressBar1.Name = "textProgressBar1";
            this.textProgressBar1.ProgressColor = System.Drawing.Color.LightGreen;
            this.textProgressBar1.Size = new System.Drawing.Size(261, 23);
            this.textProgressBar1.TabIndex = 133;
            this.textProgressBar1.TextColor = System.Drawing.Color.Black;
            this.textProgressBar1.TextFont = new System.Drawing.Font("Tahoma", 10F);
            this.textProgressBar1.VisualMode = EduSatGS.ProgressBarDisplayMode.Percentage;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 669);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(683, 708);
            this.MinimumSize = new System.Drawing.Size(683, 708);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.periodbox.ResumeLayout(false);
            this.periodbox.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GPSIndicator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telemetryIndicator)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AntennaIndicator)).EndInit();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Timer tmrPhototimeout;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txbBaud;
        private System.Windows.Forms.Button btnBaglan;
        private System.Windows.Forms.Button btnPortYenile;
        private System.Windows.Forms.Label lblBaglanti;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbAcikPortlar;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox lbTelemetryStatus;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusGunZaman;
        private System.Windows.Forms.Timer tmrZaman;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnSavePhoto;
        private System.Windows.Forms.Button btnGetPhoto;
        private System.Windows.Forms.Button btnClearImage;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox periodbox;
        private System.Windows.Forms.TextBox periodB;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox GPSIndicator;
        private System.Windows.Forms.PictureBox telemetryIndicator;
        private System.Windows.Forms.PictureBox AntennaIndicator;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label ms2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private TextProgressBar textProgressBar1;
        private System.Windows.Forms.ComboBox resbox;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button OpenFileB;
        private System.Windows.Forms.Button SaveB;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox5;
    }
}

