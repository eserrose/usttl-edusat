﻿/***************************************
 * 
 * Author:          Emirhan Eser GUL
 * Organization:    USTTL
 * Version:         v1.0.0.2
 * Last update:     17.01.2019
 * 
 **************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using System.IO.Ports;
using System.Diagnostics;
using System.Threading;

namespace EduSatGS
{
    public partial class Form1 : Form
    {
        string author1 = "E. Eser GUL", author2 = "A. Kaan SARICA";
        public Form1()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        #region Form Ops.
        string programAdi = "EduSat Ground Station";
        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = programAdi;
            toolStripStatusGunZaman.Text = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
            tmrZaman.Start();
            acikportlaribul();
            addTelemetryStatus("Program Start");

            this.textBox1.Enter += new EventHandler(textBox1_Enter);
            this.textBox1.Leave += new EventHandler(textBox1_Leave);
            textBox1_SetText();
            this.textBox2.Enter += new EventHandler(textBox2_Enter);
            this.textBox2.Leave += new EventHandler(textBox2_Leave);
            textBox2_SetText();
            this.periodB.Enter += new EventHandler(periodB_Enter);
            this.periodB.Leave += new EventHandler(periodB_Leave);
            periodB_SetText();
            resbox.Text = "Resolution";
        }
        private void addTelemetryStatus(string str)
        {
            lbTelemetryStatus.Items.Add(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "--> " + str);
            lbTelemetryStatus.SelectedIndex = lbTelemetryStatus.Items.Count - 1;
            lbTelemetryStatus.SelectedIndex = -1;
        }
        #endregion

        #region Serial Port

        byte[] rxBuffer = new byte[2000000];
        Int64 rxBufferCount = 0;

        byte gelenbyte = 0;
        byte JPEGStartbyte1 = 0xFF;
        byte JPEGStartbyte2 = 0xD8;
        byte JPEGEndByte1 = 0xFF;
        byte JPEGEndByte2 = 0xD9;

        private StringBuilder serialBuffer = new StringBuilder();
        private string terminationSequence = "\r\n";
        string[] subStrings;

        int photosize = 1;
        bool PhotoReceiveStart = false;
        bool TelemetryFlag = false, GPSFlag = false;
        private void SerialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if (Flag_WaitingPhoto)
            {
                int readcount = serialPort1.BytesToRead;
                for (int i = 0; i < readcount; i++)
                {

                    if (Flag_WaitingPhoto)
                    {
                        gelenbyte = Convert.ToByte(serialPort1.ReadByte());
                        rxBuffer[rxBufferCount] = gelenbyte;
                        rxBufferCount++;

                        //First 4 bytes will be photosize
                        if((rxBufferCount==4) && (!PhotoReceiveStart))
                        {
                            photosize = BitConverter.ToInt32(rxBuffer,0);
                        }

                        if ((!PhotoReceiveStart) && (rxBuffer[rxBufferCount - 1] == JPEGStartbyte2) && (rxBuffer[rxBufferCount - 2] == JPEGStartbyte1))
                        {
                            tmrPhototimeout.Stop();
                            ReceivedPhotoString[0] = JPEGStartbyte1;
                            ReceivedPhotoString[1] = JPEGStartbyte2;
                            PhotoCount = 2;
                            PhotoReceiveStart = true;
                            addTelemetryStatus("Photo Receive Started");
                            textProgressBar1.Maximum = photosize;
                        }

                        if (PhotoReceiveStart)
                        {
                            ReceivedPhotoString[PhotoCount] = gelenbyte;
                            PhotoCount++;
                            try { textProgressBar1.Value++; }
                            catch { }
                        }

                        if ((PhotoReceiveStart) && (rxBuffer[rxBufferCount - 1] == JPEGEndByte2) && (rxBuffer[rxBufferCount - 2] == JPEGEndByte1))
                        {
                            PhotoReceiveStart = false;
                            Flag_WaitingPhoto = false;
                            addTelemetryStatus("Photo Receive Completed");
                            btnGetPhoto.Enabled = true;
                            constructReceivedPhoto();
                            textProgressBar1.Value = 0;
                            
                       }

                    }
                }
            }
            else
            {
                string Data = serialPort1.ReadExisting();
                serialBuffer.Append(Data);

                string bufferString = serialBuffer.ToString();
                int index = -1;
                do
                {
                    index = bufferString.IndexOf(terminationSequence);
                    if (index > -1)
                    {
                        string message = bufferString.Substring(0, index);
                        bufferString = bufferString.Remove(0, index + terminationSequence.Length);
                        if (TelemetryFlag)
                        {
                            if (message == "TEL_ERROR") { addTelemetryStatus("Error connecting to OBC, please perform a hard reset"); return; }
                            try
                            {
                                subStrings = message.Split(',');
                                textBox14.Text = subStrings[0].Replace("\0", string.Empty);
                                textBox13.Text = subStrings[1];
                                textBox12.Text = subStrings[2];
                                textBox11.Text = subStrings[3];
                                textBox9.Text =  subStrings[4];
                                textBox17.Text = subStrings[5];
                                textBox16.Text = subStrings[6];
                                textBox15.Text = subStrings[7];
                                textBox20.Text = subStrings[8];
                                textBox19.Text = subStrings[9];
                                textBox18.Text = subStrings[10];
                                textBox4.Text  = subStrings[11];
                                textBox23.Text = subStrings[12];
                                textBox21.Text = subStrings[13];
                                textBox26.Text = subStrings[14];
                                textBox22.Text = subStrings[15];
                                AntennaIndicator.Image = (subStrings[16]=="1"? Properties.Resources.G: Properties.Resources.R);
                                textBox38.Text = subStrings[17]; // divide by 1024
                                textBox37.Text = subStrings[18]; // divide by 1024
                                textBox36.Text = subStrings[19]; // divide by 1024
                                textBox39.Text = subStrings[20]; // divide by 1024
                                textBox43.Text = subStrings[21];
                                textBox3.Text  = subStrings[21];
                                textBox28.Text = subStrings[23] + ":" + subStrings[24] + ":" + subStrings[25];
                                textBox27.Text = subStrings[26] + "/" + subStrings[27] + "/" + subStrings[28];
                                textBox33.Text = subStrings[29] + " deg";
                                textBox32.Text = subStrings[30] + " deg";
                                textBox31.Text = subStrings[31] + " m";
                                //DecodeErrors(Encoding.ASCII.GetBytes(subStrings[21])[0]);
                                //DecodeErrors(Convert.ToByte(subStrings[21]));
                                DecodeErrors(BitConverter.GetBytes(Int32.Parse(subStrings[22]))[0]);
                                addTelemetryStatus("Telemetry Received");
                                telemetryIndicator.Image = Properties.Resources.G;
                                SaveB.Enabled = true;
                            }
                            catch
                            {
                                addTelemetryStatus("Telemetry not received");
                                telemetryIndicator.Image = Properties.Resources.R;
                            }
                            TelemetryFlag = false;
                        }
                        else if (GPSFlag)
                        {
                            try
                            {
                                string[] subStrings = message.Split(',');
                                textBox28.Text = subStrings[0] + ":" + subStrings[1] + ":" + subStrings[2];
                                textBox27.Text = subStrings[3] + "/" + subStrings[4] + "/" + subStrings[5];
                                textBox33.Text = subStrings[6] + " deg";
                                textBox32.Text = subStrings[7] + " deg";
                                textBox31.Text = subStrings[8] + " m";
                                addTelemetryStatus("GPS Achieved");
                                GPSIndicator.Image = Properties.Resources.G;
                            }
                            catch
                            {
                                addTelemetryStatus("GPS Not Achieved");
                                GPSIndicator.Image = Properties.Resources.R;
                            }
                            GPSFlag = false;
                        }
                        else
                            addTelemetryStatus(message);
                    }
                }
                while (index > -1);
                serialBuffer = new StringBuilder(bufferString);
            }

            
        }
        private void acikportlaribul()
        {
            cbAcikPortlar.Items.Clear();
            string[] acikportlar = SerialPort.GetPortNames();
            foreach (string port in acikportlar)
                cbAcikPortlar.Items.Add(port);
        }
        private void BtnBaglan_Click(object sender, EventArgs e)
        {
            if (btnBaglan.Text == "Connect")
            {
                try
                {
                    serialPort1.PortName = cbAcikPortlar.Text;
                    serialPort1.BaudRate = Convert.ToInt32(txbBaud.Text);
                    serialPort1.Open();
                    lblBaglanti.Text = serialPort1.PortName;
                    btnBaglan.Text = "Stop";
                    btnGetPhoto.Enabled = true;
                    button2.Enabled = true;
                    button3.Enabled = true;
                    button4.Enabled = true;
                    button5.Enabled = true;
                    button6.Enabled = true;
                    button12.Enabled = true;
                    button13.Enabled = true;
                    button14.Enabled = true;
                }
                catch
                {
                    MessageBox.Show("Port Open Error", programAdi, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (btnBaglan.Text == "Stop")
            {
                serialPort1.Close();

                cbAcikPortlar.Text = "";
                lblBaglanti.Text = "Disconnected";
                btnBaglan.Text = "Connect";

                acikportlaribul();
            }
        }

        private void BtnPortYenile_Click(object sender, EventArgs e)
        {
            acikportlaribul();
        }
        #endregion

        #region Photo Related Stuff
        bool Flag_WaitingPhoto=false;
        byte[] ReceivedPhotoString= new byte[3000000];
        Image ReceivedPhoto;
        Int64 PhotoCount = 0;
        private void BtnGetPhoto_Click(object sender, EventArgs e)
        {

            serialPort1.Write("tphoto");

            Array.Clear(ReceivedPhotoString, 0, ReceivedPhotoString.Length);
            Array.Clear(rxBuffer, 0, (int)rxBufferCount);
            rxBufferCount = 0;
            photoTimeOutCounter = 0;
            Flag_WaitingPhoto = true;
            tmrPhototimeout.Start();
            btnGetPhoto.Enabled = false;
            addTelemetryStatus("Photo Send Command");
        }

        private void constructReceivedPhoto()
        {
            try
            {
                ReceivedPhoto = (Bitmap)((new ImageConverter()).ConvertFrom(ReceivedPhotoString));
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureBox1.Image = ReceivedPhoto;
                btnSavePhoto.Enabled = true;
                btnClearImage.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Error Constructing Photo", programAdi, MessageBoxButtons.OK, MessageBoxIcon.Error);
                addTelemetryStatus("Photo Constructing Error");
            }
        }

        int photoTimeOutCounter = 0;
        private void TmrPhototimeout_Tick(object sender, EventArgs e)
        {
            photoTimeOutCounter++;
            if (photoTimeOutCounter >= 20)
            {
                tmrPhototimeout.Stop();
                photoTimeOutCounter = 0;
                Flag_WaitingPhoto = false;
                Array.Clear(ReceivedPhotoString, 0, ReceivedPhotoString.Length);
                MessageBox.Show("Photo Receive Timeout", programAdi, MessageBoxButtons.OK, MessageBoxIcon.Error);
                addTelemetryStatus("Photo Receive Timed Out");
                btnGetPhoto.Enabled = true;
            }
        }
        private void BtnSavePhoto_Click(object sender, EventArgs e)
        {
            //saveFileDialog1.Filter = "JPeg Image|*.jpg*|Bitmap Image|*.bmp|Gif Image|*.gif*";
            saveFileDialog1.Filter = "jpeg Image|*.jpg*";
            saveFileDialog1.Title = "Save an Image File";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    ReceivedPhoto = pictureBox1.Image;
                    ReceivedPhoto.Save(saveFileDialog1.FileName + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    addTelemetryStatus("Photo Saved: \"" + Path.GetFileNameWithoutExtension(saveFileDialog1.FileName) + "\"");
                }
                catch
                {
                    addTelemetryStatus("Error saving photo");
                }
            }

        }

        private void BtnClearImage_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = null;
            btnSavePhoto.Enabled = false;
            btnClearImage.Enabled = false;
        }
        #endregion

        #region Timer
        private void TmrZaman_Tick(object sender, EventArgs e)
        {
            toolStripStatusGunZaman.Text = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
        }
        #endregion

        #region Set Message Command
        protected void textBox1_SetText()
        {
            this.textBox1.Text = "Type your new message here...";
            textBox1.ForeColor = Color.Gray;
            textBox1.Font = new Font(textBox1.Font, FontStyle.Italic);
            button7.Enabled = false;
        }
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button7.PerformClick();
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
            else
                button7.Enabled = true;
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if(textBox1.Text == "") button7.Enabled = false;
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox1.ForeColor = Color.Black;
            textBox1.Font = new Font(textBox1.Font, FontStyle.Regular);
        }
        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "")
                textBox1_SetText();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort1.Write("messge\n");
                string message = RemoveSpecialCharacters(textBox1.Text.ToLower());
                serialPort1.Write(message);
                addTelemetryStatus("Message Set to " + message);
            }
            catch { MessageBox.Show("Not Connected"); }
        }
        #endregion

        #region Reaction Wheel Commands
        string rw = "";
        bool buttonstate = false;
        protected void textBox2_SetText()
        {
            this.textBox2.Text = "Enter between 0 to 3400";
            textBox2.ForeColor = Color.Gray;
            textBox2.Font = new Font(textBox1.Font, FontStyle.Italic);
            button8.Enabled = false;
        }
        private void textBox2_Enter(object sender, EventArgs e)
        {
            textBox2.Text = "";
            textBox2.ForeColor = Color.Black;
            textBox2.Font = new Font(textBox1.Font, FontStyle.Regular);
        }
        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text.Trim() == "")
                textBox2_SetText();
        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.Text == "") { button8.Enabled = false; button9.Enabled = false; }
        }

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            if (!Int32.TryParse(textBox2.Text, out int x)) return;
            if (x <= 3400 && x >= 0)
            {
                button8.Enabled = true;
                if (radioButton1.Checked || radioButton2.Checked) button9.Enabled = true;
            }
            else
            {
                button8.Enabled = false;
                button9.Enabled = false;
            }
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            int rpm;
            rw = "";
            if (radioButton1.Checked) rw += "-";
            else if (radioButton2.Checked) rw += "+";
            else { MessageBox.Show("Select Direction"); return; }
            rpm = Int32.Parse(textBox2.Text);
            rw += (rpm/2).ToString().PadLeft(4, '0');
         
            try { serialPort1.Write('r'+ rw); }
            catch { MessageBox.Show("Not Connected"); return; }

            rw = rpm.ToString().PadLeft(4, '0');
            Thread.Sleep(500);

            try { serialPort1.Write('r' + rw); }
            catch { MessageBox.Show("Not Connected"); return; }

            addTelemetryStatus("Reaction Wheel Spinning with " + rw + " RPM");
            button9.Enabled = false;
            button10.Enabled = true;
            button11.Enabled = true;

        }

        private void button10_Click(object sender, EventArgs e)
        {

            try { serialPort1.Write("r+0000"); }
            catch { MessageBox.Show("Not Connected"); return; }

            button10.Enabled = false;
            button9.Enabled = true;
            button11.Enabled = false;
            addTelemetryStatus("Reaction Wheel Stopped");

        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked || radioButton2.Checked) button9.Enabled = true;
            buttonstate = true;
            addTelemetryStatus("Speed set to: " + textBox2.Text);
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if(buttonstate) button9.Enabled = true;
            buttonstate = false;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if(buttonstate) button9.Enabled = true;
            buttonstate = false;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                if (rw.ElementAt<char>(0) == '+')
                    rw = '-' + rw.Remove(0, 1);
                else
                    rw = '+' + rw.Remove(0, 1);

                serialPort1.Write('r' + rw);
            }
            catch { MessageBox.Show("Not Connected"); return; }
            addTelemetryStatus("Reaction Wheel Reversed");
        }
        #endregion

        #region Buzzer Command
        private void button6_Click(object sender, EventArgs e)
        {
            try 
            { 
                serialPort1.Write("buzzon");
                addTelemetryStatus("Buzzer On");
            }
            catch { MessageBox.Show("Not Connected"); }
        }
        #endregion

        #region Telemetry Command
        private void button2_Click(object sender, EventArgs e)
        {
            try { serialPort1.Write("tlmtry");}
            catch { MessageBox.Show("Not Connected"); return; }

            addTelemetryStatus("Telemetry Requested");
            telemetryIndicator.Image = Properties.Resources.Y;
            telemetryIndicator.Visible = true;
            TelemetryFlag = true;
        }
        #endregion

        #region GPS Command
        private void button5_Click(object sender, EventArgs e)
        {
            try { 
                serialPort1.Write("gpsdat"); 
                addTelemetryStatus("GPS Data Requested"); 
                GPSFlag = true;
                GPSIndicator.Image = Properties.Resources.Y;
                GPSIndicator.Visible = true;
            }
            catch { MessageBox.Show("Not Connected"); }
        }
        #endregion

        #region Period Command

        protected void periodB_SetText()
        {
            this.periodB.Text = "Enter btwn 30-60000 secs";
            periodB.ForeColor = Color.Gray;
            periodB.Font = new Font(periodB.Font, FontStyle.Italic);
            button13.Enabled = false;
        }
        private void periodB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button13.PerformClick();
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
        }
        private void periodB_TextChanged(object sender, EventArgs e)
        {
            if (periodB.Text == "") { button13.Enabled = false;}
        }

        private void periodB_Enter(object sender, EventArgs e)
        {
            periodB.Text = "";
            periodB.ForeColor = Color.Black;
            periodB.Font = new Font(periodB.Font, FontStyle.Regular);
        }
        private void periodB_Leave(object sender, EventArgs e)
        {
            if (periodB.Text.Trim() == "")
                periodB_SetText();
        }

        private void periodB_KeyUp(object sender, KeyEventArgs e)
        {
            if (!Int32.TryParse(periodB.Text, out int x)) return;
            button13.Enabled = (x <= 60000 && x >= 30);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (!Int32.TryParse(periodB.Text, out int x) || x<=0) return;
            try
            {
                serialPort1.Write("p"+ periodB.Text.PadLeft(5,'0')+ "\n");
            }
            catch
            { MessageBox.Show("Not Connected"); return; }

            addTelemetryStatus("Period set to " + periodB.Text + " seconds");
            periodB.Text = "";
        }
        #endregion

        #region Soft Reset Command
        private void button12_Click(object sender, EventArgs e)
        {
            try { serialPort1.Write("sreset"); }
            catch { MessageBox.Show("Not Connected"); }
            addTelemetryStatus("System Reset");
        }
        #endregion

        #region Open Antenna & Set Resolution Commands
        private void button14_Click(object sender, EventArgs e)
        {
            try { serialPort1.Write("openan"); }
            catch { MessageBox.Show("Not Connected"); return; }
            addTelemetryStatus("Antenna Opened");
        }

        private void button15_Click(object sender, EventArgs e)
        {

            try { serialPort1.Write("cares"+(resbox.SelectedIndex+(int)1)); }
            catch { MessageBox.Show("Not Connected"); return; }
            addTelemetryStatus("Resolution Set");
            resbox.Items.Insert(0, "Resolution");
            resbox.Text = "Resolution";
            button15.Enabled = false;

        }

        private void resbox_DropDown(object sender, EventArgs e)
        {
            resbox.Items.Remove("Resolution");
        }

        private void resbox_DropDownClosed(object sender, EventArgs e)
        {
            button15.Enabled = resbox.SelectedIndex > -1;
        }


        #endregion

        #region Clear Button
        private void button3_Click(object sender, EventArgs e)
        {
            textBox14.Text = "";
            textBox13.Text = "";
            textBox12.Text = "";
            textBox11.Text = "";
            textBox9.Text = "";
            textBox17.Text = "";
            textBox16.Text = "";
            textBox15.Text = "";
            textBox20.Text = "";
            textBox19.Text = "";
            textBox18.Text = "";
            textBox23.Text = "";
            textBox21.Text = "";
            textBox26.Text = "";
            textBox22.Text = "";
            AntennaIndicator.Image = Properties.Resources.R;
            textBox38.Text = "";
            textBox37.Text = "";
            textBox36.Text = "";
            textBox39.Text = "";
            textBox43.Text = "";
            textBox28.Text = "";
            textBox27.Text = "";
            textBox33.Text = "";
            textBox32.Text = "";
            textBox31.Text = "";
            textBox4.Text = "";
            SaveB.Enabled = false;
            foreach (int i in Enumerable.Range(0, 8)) checkedListBox1.SetItemCheckState(i, CheckState.Unchecked);
        }


        #endregion

        #region Telemetry Save
        string filePath;
        private void SaveB_Click(object sender, EventArgs e)
        {
           
            saveFileDialog1.Filter = "CSV File|*.csv*";
            saveFileDialog1.Title = "Save a CSV File";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    filePath = saveFileDialog1.FileName + ".csv";
                    string[] newLine = new string[32];
                    string[] type = {"Temperature","Pressure","Accelerometer X", "Accelerometer Y", "Accelerometer Z","Gyro X","Gyro Y","Gyro Z","Magneto X","Magneto Y","Magneto Z","Barometric Altitude","Vbat","Ibat","Vsol","Isol","Antenna Status","LDR1","LDR2","LDR3","LDR4","RW Speed","","H","M","S","D","M","Y","Lat","Lon","Altitude"};
                    string[] unit = { "C", "Pa", "m/s²", "m/s²", "m/s²", "deg/s", "deg/s", "deg/s", "deg", "deg", "deg", "m", "V", "mA", "V", "mA", "", "", "", "", "", "rpm","","", "","","", "", "", "deg", "deg", "m" };

                    var csv = new StringBuilder();
                    csv.AppendLine(toolStripStatusGunZaman.Text);

                    for (int i = 0; i < subStrings.Length; i++)
                    {
                        newLine[i] = string.Format("{0},{1},{2}", type[i], subStrings[i], unit[i]);
                        csv.AppendLine(newLine[i]);
                    }

                    File.AppendAllText(filePath, csv.ToString());
                 
                    addTelemetryStatus("Telemetry Saved: \"" + Path.GetFileNameWithoutExtension(saveFileDialog1.FileName) + "\"");
                    OpenFileB.Enabled = true;
                    
                }
                catch
                {
                    addTelemetryStatus("Error saving telemetry");
                }
            }
        }

        private void OpenFileB_Click(object sender, EventArgs e)
        {
            try { Process.Start(filePath); }
            catch { addTelemetryStatus("Can't open file"); }
        }


        #endregion

        #region Private Functions
        private void DecodeErrors(byte err)
        {
            for (int i = 0; i < 8; i++)
            {
                if ((err & (1 << i)) != 0)
                    checkedListBox1.SetItemCheckState(i, CheckState.Checked);
                else checkedListBox1.SetItemCheckState(i, CheckState.Unchecked);
            }
        }
        private static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        #endregion

        #region User Addition

        #endregion
    }
}
