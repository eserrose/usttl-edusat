#ifndef __ARDUCAM_H
#define __ARDUCAM_H

#include "OV5642_Regs.h"
#include "stdbool.h"
#include "SDLibrary.h"

#define ArduCam_System_Frequency	24000000	//Hz

#define JPEG	1

#define pgm_read_word(x)        ( ((*((unsigned char *)x + 1)) << 8) + (*((unsigned char *)x)))

#define ArduCam_I2C_Address	0x78 //3C

//---I2C Addresses---//
#define ArduCam_Address_RADLMT	0xFF
#define ArduCam_Address_SCSYS0	0x3008
#define ArduCam_Address_DevID		0x300A	//(2 Bytes)	(0x56 0x42)

//---SPI Addresses---//
#define ArduCam_Address_Arduchip_TEST1						0x00
#define ArduCam_Address_Arduchip_CapCtl						0x01
#define ArduCam_Address_Arduchip_TIM							0x03
#define ArduCam_Address_Arduchip_FIFO							0x04
#define ArduCam_Address_Arduchip_FIFOBurstRead		0x3C
#define ArduCam_Address_Arduchip_FIFOSingleRead		0x3D
#define ArduCam_Address_Arduchip_Version					0x40
#define ArduCam_Address_Arduchip_TRIG							0x41
#define ArduCam_Address_Arduchip_FIFOSize					0x42

//---FIFOs---//
#define FIFO_CLEAR_MASK    			0x01
#define FIFO_START_MASK    			0x02
#define FIFO_WRPTR_RST_MASK     0x20
#define FIFO_RDPTR_RST_MASK     0x10

//---Triggers---//
#define VSYNC_MASK         		0x02		//VSYNC Active Low
//#define SHUTTER_MASK       		0x02
#define CAP_DONE_MASK      		0x08

//---Test Patterns---//
#define Color_bar                      0
#define Color_square                   1
#define BW_square                      2
#define DLI                            3

//---JPEG Sizes---//
typedef enum{			//JPEG Size
	JS_Not_Set,
	OV5642_320x240,		//320x240
	OV5642_640x480,		//640x480
	OV5642_1024x768,	//1024x768
	OV5642_1280x960,	//1280x960
	OV5642_1600x1200,	//1600x1200
	OV5642_2048x1536,	//2048x1536
	OV5642_2592x1944,	//2592x1944
//	OV5642_1920x1080	//Bu �zellik kamera da yok ekstra konmus(Sanirim video i�in var)
}JPEG_Size_E;

typedef enum{			//Light Mode
	LM_Not_Set,
	Advanced_AWB,
	Simple_AWB,
	Manual_day,
	Manual_A,
	Manual_cwf,
	Manual_cloudy
}Light_Mode_E;

typedef enum{			//Color Saturation Level
	CSL_Not_Set,
	Saturation4,
	Saturation3,
	Saturation2 ,
	Saturation1,
	Saturation0,
	Saturation_1,
	Saturation_2,
	Saturation_3,
	Saturation_4
}Color_Saturation_E;

typedef enum{			//Brightness
	B_Not_Set,
	Brightness4,
	Brightness3,
	Brightness2,
	Brightness1,
	Brightness0,
	Brightness_1,
	Brightness_2,
	Brightness_3,
	Brightness_4,
}Brightness_E;

typedef enum{			//Contrast
	C_Not_Set,
	Contrast4,
	Contrast3,
	Contrast2,
	Contrast1,
	Contrast0,
	Contrast_1,
	Contrast_2,
	Contrast_3,
	Contrast_4
}Contrast_E;

typedef enum{			//Hue Degree
	H_Not_Set,
	degree_180,
	degree_150,
	degree_120,
	degree_90,
	degree_60,
	degree_30,
	degree_0,
	degree30,
	degree60,
	degree90,
	degree120,
	degree150
}Hue_E;

typedef enum{			//Special Effect
	SE_Not_Set,
	Antique,
	Bluish,
	Greenish,
	Reddish,
	BW,
	Negative,
	BWnegative,
	SE_Normal,
	Sepia,
	Overexposure,
	Solarize,
	Blueish,
	Yellowish
}Special_Effect_E;

typedef enum{			//Exposure Level
	EL_Not_Set,
	Exposure_17_EV,		//-1.7
	Exposure_13_EV,		//-1.3
	Exposure_10_EV,
	Exposure_07_EV,
	Exposure_03_EV,
	Exposure_default,
	Exposure03_EV,
	Exposure07_EV,
	Exposure10_EV,
	Exposure13_EV,		//1.3
	Exposure17_EV			//1.7
}Exposure_Level_E;

typedef enum{			//Sharpness
	S_Not_Set,
	Auto_Sharpness_default,
	Auto_Sharpness1,
	Auto_Sharpness2,
	Manual_Sharpnessoff,
	Manual_Sharpness1,
	Manual_Sharpness2,
	Manual_Sharpness3,
	Manual_Sharpness4,
	Manual_Sharpness5
}Photo_Sharpness_E;

typedef enum{			//Mirror And/Or Flip
	MF_Not_Set,
	MIRROR,
	FLIP,
	MIRROR_FLIP,
	MF_Normal
}Mirror_Flip_E;

typedef enum{			//Compress Quality
	CQ_Not_Set,
	high_quality,
	default_quality,
	low_quality
}Photo_Quality_E;

typedef struct{		//Capture Settings
	JPEG_Size_E JPEG_Size;
	Light_Mode_E Light_Mode;
	Color_Saturation_E Color_Saturation;
	Brightness_E Brightness;
	Contrast_E Contrast;
	Hue_E Hue;
	Special_Effect_E Special_Effect;
	Exposure_Level_E Exposure_Level;
	Photo_Sharpness_E Sharpness;
	Mirror_Flip_E MirrorFlip;
	Photo_Quality_E Quality;
}Capture_Settings_T;

typedef struct{		//Frame exposure
	uint16_t MaxLine;
	uint32_t shutter;			//Unit: Tline	(Line Period)
	uint32_t shutterSpeed;
}Frame_Exposure_T;

typedef struct{		//Capture
	uint8_t Frame_Count;
	uint32_t FIFO_Size;
}Capture_T;

typedef struct {	//Arducam
	uint16_t Device_ID;
	uint8_t Arduchip_Version;
	uint8_t Init_Status;
	Frame_Exposure_T FrameExposureSettings;
	Capture_Settings_T CaptureSettings;
	Capture_T Capture;
}Arducam_T;

extern Arducam_T Cam1;

void ArduCam_Init(Arducam_T *cam);
void ArduCam_SWReset(void);
void ArduCam_HardResetArduchip(void);
void ArduCam_Read_DeviceID(Arducam_T *cam);
void ArduCam_Read_ArduchipVersion(uint8_t *ver);
void ArduCam_Capture(void);
void ArduCam_Read_CapturedPhoto(Arducam_T *cam);
//---ArduChip Base Functions---//
void ArduCam_Flush_FIFO(void);
void ArduCam_StartCapture(void);
void ArduCam_Clear_FIFOFlag(void);
uint8_t ArduCam_Read_FIFO(void);
void ArduCam_Read_FIFOSize(Arducam_T *cam);
void ArduCam_Set_CaptureFrameNumber(Arducam_T *cam, uint8_t frm);
void ArduCam_Reset_ReadPointer(Arducam_T *cam);
void ArduCam_BurstReadCapture(Arducam_T *cam);
//---Capture Settings---//
void ArduCam_Set_JPEGSize(Arducam_T *cam, uint8_t size);
void ArduCam_Set_LightMode(Arducam_T *cam, uint8_t mode);
void ArduCam_Set_ColorSaturation(Arducam_T *cam, uint8_t sat);
void ArduCam_Set_Brightness(Arducam_T *cam, uint8_t brightness);
void ArduCam_Set_Contrast(Arducam_T *cam, uint8_t contrast);
void ArduCam_Set_Hue(Arducam_T *cam, uint8_t degree);
void ArduCam_Set_SpecialEffect(Arducam_T *cam, uint8_t effect);
void ArduCam_Set_ExposureLevel(Arducam_T *cam, uint8_t level);
void ArduCam_Set_Sharpness(Arducam_T *cam, uint8_t sharpness);
void ArduCam_Set_MirrorAndFlip(Arducam_T *cam, uint8_t mirror_flip);
void ArduCam_Set_CompressQuality(Arducam_T *cam, uint8_t quality);
void ArduCam_Set_TestPattern(Arducam_T *cam, uint8_t pattern);
//---Frame Exposure---//
void Arducam_InitExposure(void);
void ArduCam_Enable_ManualExposureControl(Arducam_T *cam);
void Arducam_Set_MaxLines(Arducam_T *cam, uint16_t maxLines);
void Arducam_Set_ExposureValue(Arducam_T *cam, uint32_t expVal);
void ArduCam_Set_PCLKValue(Arducam_T *cam, uint8_t val);
uint32_t ArduCam_Get_ExposureTime(Arducam_T *cam);
uint32_t ArduCam_Set_ExposureTime(Arducam_T *cam, uint16_t desExp);
void ArduCam_Get_ShutterLinePeriod(Arducam_T *cam);
//---SPI, I2C Register Read/Write Functions--//
void ArduCam_RegisterUpdate(const struct sensor_reg reglist[]);
void ArduCam_WriteRegister(uint8_t writeAdd, uint8_t writeVal);
void ArduCam_ReadRegister(uint8_t readAdd, uint8_t *readOut, uint8_t readLength);

void writeI2CMaster(uint8_t devAddr, uint16_t writeAddr, uint8_t *data, uint8_t length);
void readI2CMaster(uint8_t devAddr, uint16_t readAddr, uint8_t readLength, uint8_t *readOut);
void writeI2CMasterByte(uint8_t devAddr, uint16_t addr, uint8_t data);
void readI2CMasterByte(uint8_t devAddr, uint16_t addr, uint8_t *data);
void writeSPImaster(uint8_t writeAdd, uint8_t *writeIn, uint8_t writeLength);
void readSPImaster(uint8_t readAdd, uint8_t *readOut, uint8_t readLength);

extern SD_Card_Result SD_Write_Byte(SD_Card* DataStruct, uint8_t byte);
extern SD_Card_Result SD_OpenFile(SD_Card* DataStruct);
extern SD_Card_Result SD_CloseFile(SD_Card* DataStruct);

#endif

/**************************END OF FILE***************************/
