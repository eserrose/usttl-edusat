/****************************************************************
 * SD Card Library in Embedded C Language
 *
 * Organisation		:	ITU USTTL
 * Author					: Emirhan Eser GUL
 * Date						: 02.10.2019
 * Target MCU			: STM32F407VETx
 * Target Module	: SD Card
 * Interface(s)		: SPI
 * Version				: v1.0
 *
 *Note: Requires fatfs library from Middlewares
 ****************************************************************/
 
/****************************************************************
 * Revision History
 *
 * v1.0 || 02.10.2019 || Eser G�L || Initial Release
 *
 ****************************************************************/
 
#ifndef __SDLIBRARY_H
#define __SDLIBRARY_H

#include "stm32f4xx_hal.h"
#include "fatfs.h"


/**
 * @brief  SD Card result enumeration
 */
 
typedef enum  {
	SD_Card_Result_Ok = 0x00,          /*!< Everything OK */
	SD_Card_Result_Error,              /*!< Unknown error */
	SD_Card_Result_MountUnsuccessful,  /*!< SD Card not found */
	SD_Card_Result_InsufficientSpace,  /*!< Not enough free space in SD Card*/
	SD_Card_Result_FileOpenError,			 /*!< File cannot be opened*/
	SD_Card_Result_FileCloseError			 /*!< File cannot be closed*/
} SD_Card_Result;

typedef struct  {
	/* Private */
	FATFS fs; 								/*Sd Card instance. */
	FATFS *pfs; 							/* Pointer to Sd Card. */
	FIL fil,dummy;    				/* File */
	FRESULT fres; 						/* Free space */
	DWORD fre_clust; 					/* Free Cluster */ 
	uint32_t total, free1; 
	/* Public */
	char buffer[3][60];			/* Buffer used to write to SD Card*/
	char filename[13];			/* Name of the file*/
} SD_Card;

typedef enum  {
	TextFile = 0x00, 
	ImageFile              
} File_Type;


/**
 * @brief  Initializes SD Card for file writing by mounting and checking free space
 * @param  *DataStruct: Pointer to @ref SD_Card structure
 * @retval Member of @ref SD_Card_Result:
 *            - SD_Card_Result_Ok: everything is OK
 *            - Other: in other cases
 */
SD_Card_Result SD_Init(SD_Card* DataStruct);

/**
 * @brief  Prepares filename according to data type
 * @param  *DataStruct: Pointer to @ref SD_Card structure, type of file
 * @retval Member of @ref SD_Card_Result:
 *            - SD_Card_Result_Ok: everything is OK
 *            - Other: in other cases
 */
SD_Card_Result SD_PrepareFile(SD_Card* DataStruct,File_Type type);

/**
 * @brief  Writes given data to SD Card 
 * @param  *DataStruct: Pointer to @ref SD_Card structure, data to write
 * @retval Member of @ref SD_Card_Result:
 *            - SD_Card_Result_Ok: everything is OK
 *            - Other: in other cases
 */
SD_Card_Result SD_Write_String(SD_Card* DataStruct, char* data);

/**
 * @brief  Writes one byte data to SD Card
 * @note   Use this with openfile and closefile
 * @param  *DataStruct: Pointer to @ref SD_Card structure
 * @retval Member of @ref SD_Card_Result:
 *            - SD_Card_Result_Ok: everything is OK
 *            - Other: in other cases
 */
SD_Card_Result SD_Write_Byte(SD_Card* DataStruct, uint8_t byte);

/**
 * @brief  Reads data from SD Card and stores in buffer
 * @param  *DataStruct: Pointer to @ref SD_Card structure,
 * @retval Member of @ref SD_Card_Result:
 *            - SD_Card_Result_Ok: everything is OK
 *            - Other: in other cases
 */
SD_Card_Result SD_Read(SD_Card* DataStruct);


/**
 * @brief  Deinitializes SD Card by unmounting
 * @param  *DataStruct: Pointer to @ref SD_Card structure
 * @retval Member of @ref SD_Card_Result:
 *            - SD_Card_Result_Ok: everything is OK
 *            - Other: in other cases
 */
SD_Card_Result SD_Deinit(SD_Card* DataStruct);

/**
 * @brief  Opens file to write/read
 * @param  *DataStruct: Pointer to @ref SD_Card structure
 * @retval Member of @ref SD_Card_Result:
 *            - SD_Card_Result_Ok: everything is OK
 *            - Other: in other cases
 */
SD_Card_Result SD_OpenFile(SD_Card* DataStruct);

/**
 * @brief  Closes open file
 * @param  *DataStruct: Pointer to @ref SD_Card structure
 * @retval Member of @ref SD_Card_Result:
 *            - SD_Card_Result_Ok: everything is OK
 *            - Other: in other cases
 */
SD_Card_Result SD_CloseFile(SD_Card* DataStruct);





#endif
