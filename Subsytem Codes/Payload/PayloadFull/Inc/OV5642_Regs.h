#ifndef __OV5642_REGS_H
#define __OV5642_REGS_H

#include "stm32f4xx_hal.h"

struct sensor_reg {
  uint16_t reg;
  uint16_t val;
};

extern const struct sensor_reg ov5642_dvp_fmt_jpeg_qvga[];
extern const struct sensor_reg ov5642_dvp_fmt_global_init[];
extern const struct sensor_reg ov5642_res_1080P[];
extern const struct sensor_reg OV5642_1080P_Video_setting[];
extern const struct sensor_reg OV5642_JPEG_Capture_QSXGA[];
extern const struct sensor_reg OV5642_RGB_QVGA[];
extern const struct sensor_reg ov5642_320x240[];
extern const struct sensor_reg ov5642_640x480[];
extern const struct sensor_reg ov5642_1024x768[];
extern const struct sensor_reg ov5642_1280x960[];
extern const struct sensor_reg ov5642_1600x1200[];
extern const struct sensor_reg ov5642_2048x1536[];
extern const struct sensor_reg ov5642_2592x1944[];
extern const struct sensor_reg OV5642_QVGA_Preview[];

#endif
