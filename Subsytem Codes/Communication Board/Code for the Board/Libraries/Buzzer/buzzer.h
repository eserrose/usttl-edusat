/****************************************************************
 * Buzzer Library for Blue Pill
 *
 * Organisation		:	ITU USTTL
 * Author		: Emirhan Eser GUL
 * Date			: 10.09.2019
 * Target MCU		: STM32F103C8
 * Target Module	: Buzzer
 * Interface(s)		: GPIO
 * Version		: v1.1
 *
 ****************************************************************/
 
/****************************************************************
 * Revision History
 *
 * v1.0 || 10.09.2019 || Eser G�L || Initial Release
 *
 ****************************************************************/

#ifndef __BUZZER_H
#define __BUZZER_H

#include "Arduino.h"
#define BUZZER_PIN PB15

  //Buzzer settings

void mors(char);
void play_morse(String);
void MorseOff(int);
void MorseDash();
void MorseDot();

#endif /* __BUZZER_H */