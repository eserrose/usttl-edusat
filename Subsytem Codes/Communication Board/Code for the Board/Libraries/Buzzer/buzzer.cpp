#include "buzzer.h"

uint8_t es=200;  //end of symbol
uint16_t next = 600;  //end of letter
uint8_t dot = 200; //dot
uint16_t dash = 600;  // dash
uint16_t fin = 1400; //end of words
uint16_t note = 1200; //frequency for notes


void mors(char x)
{
  switch (x) {
  	  case 'a':	
		MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(next);
		break;
	  case 'b':
		MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(next);
		break;
	  case 'c':
	    MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(next);
		break;
	  case 'd':
		MorseDash();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(next);
		break;
	  case 'e':
		MorseDot();
		MorseOff(next);
		break;
	  case 'f':
	    MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(next);
		break;
	  case 'g':
		MorseDash();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(next);
		break;
	  case 'h':
	    MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(next);
		break;
	  case 'i':
	    MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(next);
		break;
	  case 'j':
	    MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDash();
		MorseOff(next);
		break;
      case 'k':
	    MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(next);
		break;
	  case 'l':
	    MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(next);
		break;
      case 'm':
	    MorseDash();
		MorseOff(es);
		MorseDash();
		MorseOff(next);
		break;
	  case 'n':
	    MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(next);
		break;
	  case 'o':
	    MorseDash();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDash();
		MorseOff(next);
		break;
	  case 'p':
	    MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(next);
		break;
	  case 'q':
	    MorseDash();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(next);
		break;
	  case 'r':
	    MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(next);
		break;
	  case 's':
	    MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(next);
		break;
	  case 't':
	    	MorseDash();
		MorseOff(next);
		break;
	  case 'u':
	    MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(next);
		break;
	  case 'v':
	    MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(next);
		break;
	  case 'w':
	    MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDash();
		MorseOff(next);
		break;
	  case 'x':
	    MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(next);
		break;
	  case 'y':
	    MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDash();
		MorseOff(next);
		break;
	  case 'z':
	    MorseDash();
		MorseOff(es);
		MorseDash();
		MorseOff(es);
		MorseDot();
		MorseOff(es);
		MorseDot();
		MorseOff(next);
		break;
  default:
    Serial.println("wrong input");
  }
}

 void play_morse(String command){

   uint8_t len = command.length();
   char temp[100];
   command.toCharArray(temp,100);

   for(uint8_t i = 0;i<len;i++)
   {
      mors(temp[i]);
   }

 }


//DOT
void MorseDot()
{
  tone(BUZZER_PIN, note, dot);	// start playing a tone
  delay(dot);             	// hold in this position
}

// DASH
void MorseDash()
{

  tone(BUZZER_PIN, note, dash);	// start playing a tone
  delay(dash);               // hold in this position
}

// Turn Off
void MorseOff(int delayTime)
{
  noTone(BUZZER_PIN);	       	   	// stop playing a tone
  delay(delayTime);            	// hold in this position
}
