/****************************************************************/
 /* eduSat Communication Board Code
 *
 * Organisation   : ITU USTTL
 * Author   : Emirhan Eser GUL
 * Date     : 05.09.2019
 * Target MCU   : STM32F103C8
 * Interface(s)   : UART-> OBC+GPS+Dorji,GPIO-> Buzzer
 * Version    : v1.5
 *
 ****************************************************************/
 
/****************************************************************
 * Revision History
 *
 * v0.1 || 05.09.2019 || Eser GUL || Initial Release
 * v0.2 || 07.09.2019 || Eser GUL || Algorithm Updated
 * v0.3 || 10.09.2019 || Eser GUL || Buzzer library included
 * v0.4 || 08.10.2019 || Eser GUL || New commands added
 * v1.0 || 09.12.2019 || Eser GUL || System fully operational.
 * v1.1 || 10.12.2019 || Eser GUL || Test and fixes
 * v1.2 || 18.12.2019 || Eser GUL || New operations implemented
 * v1.3 || 19.12.2019 || Eser GUL || Soft Reset added
 * v1.4 || 25.12.2019 || Eser GUL || Telemetry size changed
 * v1.5 || 27.12.2019 || Eser GUL || Telemetry size changed
 ****************************************************************
 * To do: Change the 60 into the actual telemetry size
 ****************************************************************/

#include <TinyGPS.h> //Library for GPS
#include <buzzer.h>  //Library for Buzzer

#define GPSBaud 9600 //baudrate of gps commun.

//Periodic interval
unsigned long starttime;  
unsigned long currenttime;
const unsigned long ONEMIN = 1000*60;  
const unsigned long TIMEOUT = 1000*30*1;

String morsecode = "usttl"; //Default morsecode

TinyGPSPlus gps; // The TinyGPS++ object
char gps_data[130]; //Variable for concatenating gps data

//Variables for incoming commands from the GS
char incoming_command[7]; //This should be one more than the data bcs of terminating character
String s; //String for making operations simpler
char temporary[6]; //Used to transfer the command to string
char telemetry[265]; //char array to hold telemetry data

void setup() {
  Serial1.begin(9600); //OBC
  Serial2.begin(9600);  //Dorji
  Serial3.begin(GPSBaud); //GPS
  
  starttime = millis(); //set the start time for program
   
}

void loop() {
  
  if(Serial2.available() > 0) //If data is received by Dorji
  { 
       s=Serial2.readStringUntil('\n');
       if(s=="buzzon") //Command for turning on the buzzer
      {
        play_morse(morsecode);
      }

      else if(s=="messge") //Command for changing the buzzer message
      {
        morsecode = Serial2.readStringUntil('\n');
      }

      else if(s=="gpsdat") //Command for receiving GPS data
      {
         parsing_gps_data();
         Serial2.println(gps_data);
      }

      else if(s.substring(0,1)=="r") //Command for receiving RW data
      {
        Serial1.print(s);
      }
      else if(s=="tlmtry") //Command for receiving RW data
      {
        if(request_telemetry()) send_telemetry();
        else send_error();
      }
      else if( s=="tphoto" || s=="openan" || s=="sreset" || s.substring(0,1)=="p" || s.substring(0,5)=="cares"|| s.substring(0,4)=="abdr") //Various commands
      {
        Serial1.print(s);
        if(s=="sreset") {delay(100); nvic_sys_reset();}
      } 
      else
      {     
       play_morse("no"); //play buzzer once
      }

      memset(incoming_command,0,sizeof(incoming_command)); //Empty the contents of incoming_command for the next one
  }

  if(Serial1.available()){
      while(Serial1.available())//If OBC is sending data
        { 
          Serial2.write(Serial1.read()); 
        }
  }

}

void parsing_gps_data()
{
  // This sketch displays information every time a new sentence is correctly encoded.
  while (Serial3.available() > 0)
      if (gps.encode(Serial3.read()))
        {sampling_gps_data();}
      
}

void sampling_gps_data()
{
 sprintf(gps_data, "%u,%u,%u,%u,%u,%u,%.5f,%.5f,%.5f", gps.time.hour(), gps.time.minute(), gps.time.second(), gps.date.day(), gps.date.month(), gps.date.year(),gps.location.lat(), gps.location.lng(), gps.altitude.meters());
}


bool request_telemetry()
{
  
  unsigned long counter = millis() + TIMEOUT; //start counting time
  Serial1.print("tlmtry"); //Send telemetry command to OBC
  while(!Serial1.available())
    {
      if((long)(millis() - counter) >= 0) return false; //if OBC doesnt respond in thirty seconds, return false
    }
  
   Serial1.readBytes(telemetry,140); //Read telemetry data from OBC
   parsing_gps_data(); //Update GPS data
   sprintf(telemetry, "%s,%s", telemetry,gps_data); //Combine the received data with GPS data
   return true;
}

void send_telemetry()
{
  Serial2.println(telemetry); //Send telemetry data to GS via Dorji
  memset(telemetry,0,sizeof(telemetry)); //clear the telemetry data
}

void send_error()
{
  Serial2.println("TEL_ERROR");  //Send Telemetry error to GS
}
