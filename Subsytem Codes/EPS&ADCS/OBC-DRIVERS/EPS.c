#include "EPS.h"

/*Variable to store the incoming data*/
uint8_t epsdata[26] = "00.00,00.00,00.00,00.00,0";

EPS_Status getEPSdata(){
	 if(HAL_I2C_Master_Receive(&hi2c1,EPSAddr<<1,epsdata,sizeof(epsdata),100)!=HAL_OK) return EPS_Comm_Error;
		return EPS_OK;
}

EPS_Status OpenAntennas(){
	 uint8_t command[10] = "antenna_on";
   if(HAL_I2C_Master_Transmit(&hi2c1,EPSAddr<<1,command,sizeof(command),10)!=HAL_OK) return EPS_Comm_Error;
	 return EPS_OK; 
}

EPS_Status SetAntennaBurnDuration(uint8_t dur){
	 uint8_t command[10] = "duration";
   command[9] = dur;
   if(HAL_I2C_Master_Transmit(&hi2c1,EPSAddr<<1,command,sizeof(command),10)!=HAL_OK) return EPS_Comm_Error;
	return EPS_OK;
}

EPS_Status EPS_Reset(){
	 uint8_t command[10] = "soft_reset";
   if(HAL_I2C_Master_Transmit(&hi2c1,EPSAddr<<1,command,sizeof(command),10)!=HAL_OK) return EPS_Comm_Error;
	return EPS_OK;

}
