/*
 * HMC5883L.c
 *
 *  Created on: Dec 06, 2019
 *      Author: Emirhan Eser GUL
 */

/**
 * |----------------------------------------------------------------------
 * | Copyright (C) Eser GUL, 2019
 * |----------------------------------------------------------------------
 */


#include "HMC5883L.h"
extern I2C_HandleTypeDef hi2c3;

uint8_t DataX[2];
uint8_t DataY[2];
uint8_t DataZ[2];
uint8_t RegSettingA = (uint8_t)HMC5883l_Enable_A;
uint8_t RegSettingB = (uint8_t)HMC5883l_Enable_B;
uint8_t RegSettingMR = (uint8_t)HMC5883l_MR;

void ConfigureHMC5883L(void){
	HAL_I2C_Mem_Write(&hi2c3, HMC5883l_ADDRESS, HMC5883L_RA_CONFIG_A , 1, &RegSettingA , 1, 100);
	HAL_I2C_Mem_Write(&hi2c3, HMC5883l_ADDRESS, HMC5883L_RA_CONFIG_B , 1, &RegSettingB , 1, 100);
	HAL_I2C_Mem_Write(&hi2c3, HMC5883l_ADDRESS, HMC5883L_RA_MODE , 1, &RegSettingMR , 1, 100);
	HAL_Delay(500);
}

uint16_t GetX(void){
	if(HAL_I2C_Mem_Read(&hi2c3,HMC5883l_ADDRESS,HMC5883l_ADD_DATAX_MSB_MULTI,1,DataX,2,100)!=HAL_OK) return 0xFFFF;
	return ((DataX[1]<<8) | DataX[0])/660.f;
}

uint16_t GetY(void){
	HAL_I2C_Mem_Read(&hi2c3,HMC5883l_ADDRESS,HMC5883l_ADD_DATAY_MSB_MULTI,1,DataY,2,100);
	return ((DataY[1]<<8) | DataY[0])/660.f;
}

uint16_t GetZ(void){
	HAL_I2C_Mem_Read(&hi2c3,HMC5883l_ADDRESS,HMC5883l_ADD_DATAZ_MSB_MULTI,1,DataZ,2,100);
	return ((DataZ[1]<<8) | DataZ[0])/660.f;
}

