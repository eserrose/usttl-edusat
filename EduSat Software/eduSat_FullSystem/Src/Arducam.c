/****************************************************************
 * Arducam 5MP Drivers in Embedded C Language
 *
 * Organisation		:	ITU USTTL
 * Author					: Ahmet Kaan SARICA
 * Date						: 15.05.2019
 * Target MCU			: NXP LPC824
 * Target Sensor	: OV5642
 * Target Module	: Arducam 5MP
 * Interface(s)		: I2C->Config, SPI->Command And Data
 * Version				: v1.1
 *
 * Note : Currently only supports JPEG format(Not RAW or BMP)
 ****************************************************************/
 
/****************************************************************
 * Revision History
 *
 * v1.0 || 16.07.2019 || Kaan SARICA || Initial Release
 * v1.1 || 02.09.2019 || Kaan SARICA || Manual Exposure Control Added
 * v1.2 || 27.09.2019 || Eser GUL    || Made compatible with Ertek OBC
 *
 ****************************************************************/

#include "Arducam.h"

Arducam_T Cam1;

uint8_t ArduCam_DeviceID[2]={0};
static uint8_t buf[256];
extern I2C_HandleTypeDef hi2c1;
extern SPI_HandleTypeDef hspi2;
extern SD_Card sd1;

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Init(Arducam_T *cam){
	
	uint8_t temp;
	
	//Reset the CPLD
	ArduCam_WriteRegister(0x07, 0x80);
	HAL_Delay(100);
	ArduCam_WriteRegister(0x07, 0x00);
	HAL_Delay(100);
	
	//Arduchip Test	
	ArduCam_WriteRegister(ArduCam_Address_Arduchip_TEST1, 0x42);
	ArduCam_ReadRegister(ArduCam_Address_Arduchip_TEST1, &temp, 1);

	
	//Check if camera module type is OV5642
	ArduCam_Read_DeviceID(cam);
	HAL_Delay(250);
	ArduCam_Read_ArduchipVersion(&cam->Arduchip_Version);
	
	HAL_Delay(250);
	ArduCam_SWReset();
	HAL_Delay(1000);
	ArduCam_RegisterUpdate(OV5642_QVGA_Preview);
	HAL_Delay(250);
	ArduCam_RegisterUpdate(OV5642_JPEG_Capture_QSXGA);
	HAL_Delay(250);

	ArduCam_Set_JPEGSize(cam, OV5642_320x240);
	HAL_Delay(10);
	ArduCam_Set_LightMode(cam, Advanced_AWB);
	HAL_Delay(10);
	ArduCam_Set_ColorSaturation(cam, Saturation4);
	HAL_Delay(10);
	ArduCam_Set_Brightness(cam, Brightness0);
	HAL_Delay(10);
	ArduCam_Set_Contrast(cam, Contrast2);
	HAL_Delay(10);
	ArduCam_Set_Hue(cam, degree_0);
	HAL_Delay(10);
	ArduCam_Set_SpecialEffect(cam, SE_Normal);
	HAL_Delay(10);
	ArduCam_Set_ExposureLevel(cam, Exposure_default);
	HAL_Delay(10);
	ArduCam_Set_Sharpness(cam, Manual_Sharpnessoff);
	HAL_Delay(10);
	ArduCam_Set_MirrorAndFlip(cam, MF_Normal);
	HAL_Delay(10);
	ArduCam_Set_CompressQuality(cam, high_quality);
	HAL_Delay(100);
	
	temp=0xA8;
	writeI2CMaster(ArduCam_I2C_Address, 0x3818, &temp, 1);
	HAL_Delay(100);
	temp=0x10;
	writeI2CMaster(ArduCam_I2C_Address, 0x3621, &temp, 1);
	HAL_Delay(100);
	temp=0xB0;
	writeI2CMaster(ArduCam_I2C_Address, 0x3801, &temp, 1);
	HAL_Delay(100);
	temp=0x04;
	writeI2CMaster(ArduCam_I2C_Address, 0x4407, &temp, 1);
	
	Arducam_InitExposure();
	
	HAL_Delay(100);
	ArduCam_WriteRegister(ArduCam_Address_Arduchip_TIM, VSYNC_MASK);	//VSYNC Active Low
	
	ArduCam_Clear_FIFOFlag();
	
	ArduCam_Set_CaptureFrameNumber(cam, 1);
	
	cam->Init_Status=1;
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_SWReset(void){
	
	uint8_t temp=0x80;
	
	writeI2CMaster(ArduCam_I2C_Address, ArduCam_Address_SCSYS0, &temp, 1);
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_HardResetArduchip(void){
	
	ArduCam_WriteRegister(0x06, 0x11);
	HAL_Delay(1000);
	ArduCam_WriteRegister(0x06, 0x15);
	
}

/****************************************************************	//OK
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Read_DeviceID(Arducam_T *cam){
	uint8_t tmp[2];
	readI2CMaster(ArduCam_I2C_Address, ArduCam_Address_DevID, 2, &tmp[0]);
	cam->Device_ID=(tmp[0]<<8) | tmp[1];
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Read_ArduchipVersion(uint8_t *ver){
	
	ArduCam_ReadRegister(ArduCam_Address_Arduchip_Version, ver, 1);
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Capture(void){
	
	uint8_t tmp;
	
	ArduCam_Flush_FIFO();
	ArduCam_Clear_FIFOFlag();
//	ArduCam_Set_JPEGSize(&Cam1, OV5642_320x240);
	HAL_Delay(1000);
	ArduCam_StartCapture();
	
	ArduCam_ReadRegister(ArduCam_Address_Arduchip_TRIG, &tmp, 1);
	
	while (!(tmp & CAP_DONE_MASK)) {
		ArduCam_ReadRegister(ArduCam_Address_Arduchip_TRIG, &tmp, 1);
  }
	HAL_Delay(1000);
	
	ArduCam_Read_FIFOSize(&Cam1);
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Read_CapturedPhoto(Arducam_T *cam){
	
	uint8_t temp = 0, temp_last = 0;
  uint32_t length = 0;
	uint32_t i=0;
	uint32_t k=0;
	
	
	bool is_header=false;
	
	length=cam->Capture.FIFO_Size;
	
	i=0;
	
	SD_OpenFile(&sd1);

	while(length--){
		
		temp_last=temp;
		temp=ArduCam_Read_FIFO();
		SD_Write_Byte(&sd1,temp);		

		if( (temp == 0xD9) && (temp_last == 0xFF) ){	//If find the end ,break while,
			buf[i++] = temp;		
			is_header=false;
      i = 0;
		}
		
		if(is_header){
			
			if(i<256){
				buf[i++]=temp;
			}
			else{
        i = 0;
				buf[i++] = temp;
			}
		}
		else if ((temp == 0xD8) & (temp_last == 0xFF)){
			is_header = true;
      buf[i++] = temp_last;
      buf[i++] = temp;
		}
	}
	SD_CloseFile(&sd1);
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Send_CapturedPhoto(Arducam_T *cam, bool save){
	
	uint8_t temp = 0, temp_last = 0;
  uint32_t length = 0;
	uint32_t i=0;
	uint32_t k=0;
	
	uint8_t size;
	
	bool is_header=false;
	
	length=cam->Capture.FIFO_Size;
	
	for(int8_t i=0;i<4;i++){
		size = length>>(8*i);
	  HAL_UART_Transmit(&huart4,&size,1,1);
	}
	
	i=0;
	if(save) SD_OpenFile(&sd1);
	while(length--){
		
		temp_last=temp;
		temp=ArduCam_Read_FIFO();
		
		if(save) SD_Write_Byte(&sd1,temp);	
		
		HAL_UART_Transmit(&huart4,&temp,sizeof(temp),1);
		HAL_Delay(1);

		if( (temp == 0xD9) && (temp_last == 0xFF) ){	//If find the end ,break while,
			buf[i++] = temp;		
			is_header=false;
      i = 0;
		}
		
		if(is_header){
			
			if(i<256){
				buf[i++]=temp;
			}
			else{
        i = 0;
				buf[i++] = temp;
			}
		}
		else if ((temp == 0xD8) & (temp_last == 0xFF)){
			is_header = true;
      buf[i++] = temp_last;
      buf[i++] = temp;
		}
	}
	if(save) SD_CloseFile(&sd1);
	__HAL_UART_ENABLE_IT(&huart4,UART_IT_RXNE);
}


//---ArduChip Base Functions---//
/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Flush_FIFO(void){
	
	ArduCam_WriteRegister(ArduCam_Address_Arduchip_FIFO, FIFO_CLEAR_MASK);
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_StartCapture(void){
	
	ArduCam_WriteRegister(ArduCam_Address_Arduchip_FIFO, FIFO_START_MASK);
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Clear_FIFOFlag(void){
	
	ArduCam_WriteRegister(ArduCam_Address_Arduchip_FIFO, FIFO_CLEAR_MASK);
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
uint8_t ArduCam_Read_FIFO(void){
	
	uint8_t data;
	
	//ArduCam_ReadRegister(0x3D, &data, 1);

	uint8_t yyy = 0x3D;
	uint8_t dummy = 0x00;
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_RESET);
	HAL_SPI_Transmit (&hspi2, &yyy, 1, 1);
	HAL_SPI_TransmitReceive(&hspi2,&dummy,&data,1,1);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_SET);
	
	
	return data;	
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Read_FIFOSize(Arducam_T *cam){
	
	uint8_t tempArr[3]={0x00};	
	
	ArduCam_ReadRegister(0x42, &tempArr[0], 1);
	ArduCam_ReadRegister(0x43, &tempArr[1], 1);
	ArduCam_ReadRegister(0x44, &tempArr[2], 1);
	
	cam->Capture.FIFO_Size=(((uint32_t)tempArr[2])<<16) | (((uint32_t)tempArr[1])<<8) | (((uint32_t)tempArr[0]));
}

/****************************************************************
 * @brief		
 * @param		frm	: Number of frames to capture. Between 1-7. For video frm=8(Not supported yet).
 * @return	Nothing
 * @note		Video �zelligi ilk asamada desteklenmiyor
****************************************************************/
void ArduCam_Set_CaptureFrameNumber(Arducam_T *cam, uint8_t frm){
	
	if(frm>7){//ftm=8 video �ekmek i�in bizde bu �zellik olmayacak
		frm=0;
	}
	ArduCam_WriteRegister(ArduCam_Address_Arduchip_CapCtl, frm-1);
	cam->Capture.Frame_Count=frm;
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Reset_ReadPointer(Arducam_T *cam){
	
	ArduCam_WriteRegister(ArduCam_Address_Arduchip_FIFO, FIFO_RDPTR_RST_MASK);
}

/****************************************************************	//Bunu kullanma, bu burst read �alismiyor :S :S
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_BurstReadCapture(Arducam_T *cam){
	
//	uint8_t i=0,tempArr[6]={0x00};
//	uint8_t total_ReadCycle=0;
//	
//	for(i=0; i<32; i++){
//		spiTxBuff[i]=0x00;
//		spiRxBuff[i]=0x39;
//	}

//	total_ReadCycle=cam->Capture.FIFO_Size/32;
//	if(total_ReadCycle*32<cam->Capture.FIFO_Size){
//		total_ReadCycle++;
//	}
//	
//	for(i=0; i<total_ReadCycle+6; i++){
//		
////		if(i==0)
//			spiTxBuff[0]=ArduCam_Address_Arduchip_FIFOBurstRead;
////		else
////			spiTxBuff[0]=0x00;
//		
//		XfSetup.Length = 32;
//		XfSetup.pTx = spiTxBuff;
//		XfSetup.TxCnt = 0;
//		XfSetup.pRx = spiRxBuff;
//		XfSetup.RxCnt = 0;
//		XfSetup.DataSize = 8;
//		
//		
//		Chip_SPI_RWFrames_Blocking(LPC_SPI1, &XfSetup);
//		if(i>195){
//			__nop();
//		}
//	}
	
//uint8_t temp;
	
	//Send Burst Read Command
//	spiTxBuff[0]=ArduCam_Address_Arduchip_FIFOBurstRead;
//	XfSetup.Length = 1;
//	XfSetup.pTx = spiTxBuff;
//	XfSetup.TxCnt = 0;
//	XfSetup.pRx = spiRxBuff;
//	XfSetup.RxCnt = 0;
//	XfSetup.DataSize = 8;
//	
//	Chip_SPI_WriteFrames_Blocking(LPC_SPI1, &XfSetup);
	
	//Start Reading
//	while(--(cam->Capture.FIFO_Size)){
////		Chip_SPI_ReadFrames_Blocking(LPC_SPI1, &XfSetup);
//		temp=ArduCam_Read_FIFO();
//	}
}

//---Capture Settings---//
/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Set_JPEGSize(Arducam_T *cam, uint8_t size){
	
  switch (size)
  {
    case OV5642_320x240:
      ArduCam_RegisterUpdate(ov5642_320x240);
      break;
    case OV5642_640x480:
      ArduCam_RegisterUpdate(ov5642_640x480);
      break;
    case OV5642_1024x768:
      ArduCam_RegisterUpdate(ov5642_1024x768);
      break;
    case OV5642_1280x960:
      ArduCam_RegisterUpdate(ov5642_1280x960);
      break;
    case OV5642_1600x1200:
      ArduCam_RegisterUpdate(ov5642_1600x1200);
      break;
    case OV5642_2048x1536:
      ArduCam_RegisterUpdate(ov5642_2048x1536);
      break;
    case OV5642_2592x1944:
      ArduCam_RegisterUpdate(ov5642_2592x1944);
      break;
    default:
      ArduCam_RegisterUpdate(ov5642_320x240);
      break;
  }
	cam->CaptureSettings.JPEG_Size=size;
}	

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Set_LightMode(Arducam_T *cam, uint8_t mode){
	
	switch(mode)
	{
		case Advanced_AWB:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3406 ,0x0);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5192 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5191 ,0xf8);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x518d ,0x26);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x518f ,0x42);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x518e ,0x2b);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5190 ,0x42);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x518b ,0xd0);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x518c ,0xbd);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5187 ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5188 ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5189 ,0x56);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x518a ,0x5c);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5186 ,0x1c);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5181 ,0x50);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5184 ,0x20);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5182 ,0x11);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5183 ,0x0 );	
			break;
		
		case Simple_AWB:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3406 ,0x00);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5183 ,0x80);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5191 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5192 ,0x00);
			break;
		
		case Manual_day:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3406 ,0x1 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3400 ,0x7 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3401 ,0x32);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3402 ,0x4 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3403 ,0x0 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3404 ,0x5 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3405 ,0x36);
			break;
		
		case Manual_A:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3406 ,0x1 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3400 ,0x4 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3401 ,0x88);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3402 ,0x4 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3403 ,0x0 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3404 ,0x8 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3405 ,0xb6);
			break;
		
		case Manual_cwf:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3406 ,0x1 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3400 ,0x6 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3401 ,0x13);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3402 ,0x4 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3403 ,0x0 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3404 ,0x7 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3405 ,0xe2);
			break;
		
		case Manual_cloudy:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3406 ,0x1 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3400 ,0x7 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3401 ,0x88);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3402 ,0x4 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3403 ,0x0 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3404 ,0x5 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3405 ,0x0);
			break;
		
		default :
			break; 
	}
	cam->CaptureSettings.Light_Mode=mode;
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Set_ColorSaturation(Arducam_T *cam, uint8_t sat){

	switch(sat)
	{
		case Saturation4:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5583 ,0x80);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5584 ,0x80);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x02);
		break;
		case Saturation3:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5583 ,0x70);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5584 ,0x70);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x02);
		break;
		case Saturation2:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5583 ,0x60);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5584 ,0x60);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x02);
		break;
		case Saturation1:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5583 ,0x50);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5584 ,0x50);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x02);
		break;
		case Saturation0:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5583 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5584 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x02);
		break;		
		case Saturation_1:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5583 ,0x30);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5584 ,0x30);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x02);
		break;
			case Saturation_2:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5583 ,0x20);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5584 ,0x20);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x02);
		break;
			case Saturation_3:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5583 ,0x10);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5584 ,0x10);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x02);
		break;
			case Saturation_4:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5583 ,0x00);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5584 ,0x00);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x02);
		break;
	}
	cam->CaptureSettings.Color_Saturation=sat;
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Set_Brightness(Arducam_T *cam, uint8_t brightness){
	
	switch(brightness)
	{
		case Brightness4:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5589 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x00);
		break;
		case Brightness3:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5589 ,0x30);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x00);
		break;	
		case Brightness2:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5589 ,0x20);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x00);
		break;
		case Brightness1:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5589 ,0x10);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x00);
		break;
		case Brightness0:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5589 ,0x00);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x00);
		break;	
		case Brightness_1:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5589 ,0x10);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x08);
		break;	
		case Brightness_2:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5589 ,0x20);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x08);
		break;	
		case Brightness_3:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5589 ,0x30);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x08);
		break;	
		case Brightness_4:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5589 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x08);
		break;	
	}
	cam->CaptureSettings.Brightness=brightness;
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Set_Contrast(Arducam_T *cam, uint8_t contrast){
	
	switch(contrast)
	{
		case Contrast4:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5587 ,0x30);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5588 ,0x30);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x00);
		break;
		case Contrast3:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5587 ,0x2c);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5588 ,0x2c);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x00);
		break;
		case Contrast2:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5587 ,0x28);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5588 ,0x28);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x00);
		break;
		case Contrast1:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5587 ,0x24);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5588 ,0x24);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x00);
		break;
		case Contrast0:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5587 ,0x20);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5588 ,0x20);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x00);
		break;
		case Contrast_1:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5587 ,0x1C);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5588 ,0x1C);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x1C);
		break;
		case Contrast_2:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5587 ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5588 ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x00);
		break;
		case Contrast_3:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5587 ,0x14);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5588 ,0x14);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x00);
		break;
		case Contrast_4:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5587 ,0x10);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5588 ,0x10);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x00);
		break;
	}
	cam->CaptureSettings.Contrast=contrast;
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Set_Hue(Arducam_T *cam, uint8_t degree){
	
	switch(degree)
	{
		case degree_180:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x01);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5581 ,0x80);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5582 ,0x00);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x32);
		break;
		case degree_150:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x01);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5581 ,0x6f);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5582 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x32);
		break;
		case degree_120:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x01);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5581 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5582 ,0x6f);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x32);
		break;
		case degree_90:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x01);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5581 ,0x00);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5582 ,0x80);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x02);
		break;
		case degree_60:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x01);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5581 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5582 ,0x6f);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x02);
		break;
		case degree_30:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x01);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5581 ,0x6f);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5582 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x02);
		break;
		case degree_0:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x01);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5581 ,0x80);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5582 ,0x00);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x01);
		break;
		case degree30:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x01);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5581 ,0x6f);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5582 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x01);
		break;
		case degree60:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x01);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5581 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5582 ,0x6f);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x01);
		break;
		case degree90:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x01);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5581 ,0x00);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5582 ,0x80);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x31);
		break;
		case degree120:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x01);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5581 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5582 ,0x6f);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x31);
		break;
		case degree150:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x01);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5581 ,0x6f);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5582 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x558a ,0x31);
		break;
	}
	
	cam->CaptureSettings.Hue=degree;
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Set_SpecialEffect(Arducam_T *cam, uint8_t effect){
	
	switch(effect)
	{
		case Bluish:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5585 ,0xa0);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5586 ,0x40);
		break;
		case Greenish:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5585 ,0x60);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5586 ,0x60);
		break;
		case Reddish:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5585 ,0x80);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5586 ,0xc0);
		break;
		case BW:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5585 ,0x80);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5586 ,0x80);
		break;
		case Negative:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x40);
		break;
		
			case Sepia:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0xff);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5585 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5586 ,0xa0);
		break;
		case SE_Normal:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5001 ,0x7f);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5580 ,0x00);		
		break;		
	}
	
	cam->CaptureSettings.Special_Effect=effect;
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Set_ExposureLevel(Arducam_T *cam, uint8_t level){
	
	switch(level)
	{
		case Exposure_17_EV:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a0f ,0x10);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a10 ,0x08);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1b ,0x10);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1e ,0x08);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a11 ,0x20);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1f ,0x10);
		break;
		case Exposure_13_EV:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a0f ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a10 ,0x10);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1b ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1e ,0x10);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a11 ,0x30);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1f ,0x10);
		break;
		case Exposure_10_EV:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a0f ,0x20);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a10 ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a11 ,0x41);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1b ,0x20);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1e ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1f ,0x10);
		break;
		case Exposure_07_EV:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a0f ,0x28);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a10 ,0x20);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a11 ,0x51);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1b ,0x28);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1e ,0x20);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1f ,0x10);
		break;
		case Exposure_03_EV:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a0f ,0x30);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a10 ,0x28);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a11 ,0x61);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1b ,0x30);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1e ,0x28);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1f ,0x10);
		break;
		case Exposure_default:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a0f ,0x38);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a10 ,0x30);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a11 ,0x61);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1b ,0x38);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1e ,0x30);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1f ,0x10);
		break;
		case Exposure03_EV:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a0f ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a10 ,0x38);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a11 ,0x71);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1b ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1e ,0x38);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1f ,0x10);
		break;
		case Exposure07_EV:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a0f ,0x48);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a10 ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a11 ,0x80);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1b ,0x48);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1e ,0x40);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1f ,0x20);
		break;
		case Exposure10_EV:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a0f ,0x50);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a10 ,0x48);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a11 ,0x90);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1b ,0x50);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1e ,0x48);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1f ,0x20);
		break;
		case Exposure13_EV:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a0f ,0x58);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a10 ,0x50);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a11 ,0x91);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1b ,0x58);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1e ,0x50);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1f ,0x20);
		break;
		case Exposure17_EV:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a0f ,0x60);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a10 ,0x58);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a11 ,0xa0);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1b ,0x60);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1e ,0x58);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3a1f ,0x20);
		break;
	}
	
	cam->CaptureSettings.Exposure_Level=level;
}
/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Set_Sharpness(Arducam_T *cam, uint8_t sharpness){
	
	switch(sharpness)
	{
		case Auto_Sharpness_default:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530A ,0x00);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530c ,0x0 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530d ,0xc );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5312 ,0x40);
		break;
		case Auto_Sharpness1:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530A ,0x00);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530c ,0x4 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530d ,0x18);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5312 ,0x20);
		break;
		case Auto_Sharpness2:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530A ,0x00);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530c ,0x8 );
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530d ,0x30);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x5312 ,0x10);
		break;
		case Manual_Sharpnessoff:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530A ,0x08);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x531e ,0x00);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x531f ,0x00);
		break;
		case Manual_Sharpness1:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530A ,0x08);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x531e ,0x04);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x531f ,0x04);
		break;
		case Manual_Sharpness2:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530A ,0x08);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x531e ,0x08);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x531f ,0x08);
		break;
		case Manual_Sharpness3:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530A ,0x08);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x531e ,0x0c);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x531f ,0x0c);
		break;
		case Manual_Sharpness4:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530A ,0x08);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x531e ,0x0f);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x531f ,0x0f);
		break;
		case Manual_Sharpness5:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x530A ,0x08);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x531e ,0x1f);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x531f ,0x1f);
		break;
	}
	
	cam->CaptureSettings.Sharpness=sharpness;
}
/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Set_MirrorAndFlip(Arducam_T *cam, uint8_t mirror_flip){
	
	uint8_t reg_val;
	
	switch(mirror_flip)
	{
		case MIRROR:
			readI2CMasterByte(ArduCam_I2C_Address, 0x3818,&reg_val);
			reg_val = reg_val|0x00;
			reg_val = reg_val&0x9F;
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3818 ,reg_val);
			readI2CMasterByte(ArduCam_I2C_Address, 0x3621,&reg_val);
			reg_val = reg_val|0x20;
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3621, reg_val );
		
		break;
		case FLIP:
			readI2CMasterByte(ArduCam_I2C_Address, 0x3818,&reg_val);
			reg_val = reg_val|0x20;
			reg_val = reg_val&0xbF;
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3818 ,reg_val);
			readI2CMasterByte(ArduCam_I2C_Address, 0x3621,&reg_val);
			reg_val = reg_val|0x20;
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3621, reg_val );
		break;
		case MIRROR_FLIP:
			readI2CMasterByte(ArduCam_I2C_Address, 0x3818,&reg_val);
			reg_val = reg_val|0x60;
			reg_val = reg_val&0xFF;
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3818 ,reg_val);
			readI2CMasterByte(ArduCam_I2C_Address, 0x3621,&reg_val);
			reg_val = reg_val&0xdf;
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3621, reg_val );
		break;
		case MF_Normal:
			readI2CMasterByte(ArduCam_I2C_Address, 0x3818,&reg_val);
			reg_val = reg_val|0x40;
			reg_val = reg_val&0xdF;
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3818 ,reg_val);
			readI2CMasterByte(ArduCam_I2C_Address, 0x3621,&reg_val);
			reg_val = reg_val&0xdf;
			writeI2CMasterByte(ArduCam_I2C_Address, 0x3621, reg_val );
		break;
	}
	
	cam->CaptureSettings.MirrorFlip=mirror_flip;
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Set_CompressQuality(Arducam_T *cam, uint8_t quality){
	
	switch(quality)
	{
		case high_quality:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x4407, 0x02);
			break;
		case default_quality:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x4407, 0x04);
			break;
		case low_quality:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x4407, 0x08);
			break;
	}
	
	cam->CaptureSettings.Quality=quality;
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Set_TestPattern(Arducam_T *cam, uint8_t pattern){
	
	switch(pattern)
	{
		case Color_bar:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x503d , 0x80);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x503e, 0x00);
			break;
		case Color_square:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x503d , 0x85);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x503e, 0x12);
			break;
		case BW_square:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x503d , 0x85);
			writeI2CMasterByte(ArduCam_I2C_Address, 0x503e, 0x1a);
			break;
		case DLI:
			writeI2CMasterByte(ArduCam_I2C_Address, 0x4741 , 0x4);
			break;
	}
}

//---Frame Exposure---//
/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void Arducam_InitExposure(void){
	
	//keeping in mind that[20bit exposure value] it is big-endian
	//Big-endian is an order in which the "big end" (most significant value in the sequence) is stored first (at the lowest storage address).
	ArduCam_Enable_ManualExposureControl(&Cam1);	
	Arducam_Set_MaxLines(&Cam1, 3200);
	Arducam_Set_ExposureValue(&Cam1, 0x0C800);
	ArduCam_Set_PCLKValue(&Cam1, 64);
	ArduCam_Get_ExposureTime(&Cam1);
	
}
/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Enable_ManualExposureControl(Arducam_T *cam){
	
	writeI2CMasterByte(ArduCam_I2C_Address, 0x3503 , 0x05);
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void Arducam_Set_MaxLines(Arducam_T *cam, uint16_t maxLines){
	
	uint8_t regVals[2];
	uint8_t regs[2];
	
	regVals[0]=((maxLines>>8)&0xFF);
	regVals[1]=(maxLines&0xFF);
	
	writeI2CMasterByte(ArduCam_I2C_Address, 0x350C, regVals[0]);
	writeI2CMasterByte(ArduCam_I2C_Address, 0x350D, regVals[1]);
	
	HAL_Delay(10);
	readI2CMaster(ArduCam_I2C_Address, 0x350C, 2, &regs[0]);
	
	cam->FrameExposureSettings.MaxLine=(regs[0]<<8) + regs[1];
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void Arducam_Set_ExposureValue(Arducam_T *cam, uint32_t expVal){
	
	uint8_t regVals[3]={0x00};
	uint8_t i=0;
	float shutter=0.0;
	
	if(expVal>0xFFFFF){
		return;
}
	
	regVals[0]=(expVal>>16)&0x0F;
	regVals[1]=(expVal>>8)&0xFF;
	regVals[2]=expVal&0xFF;
	

	for(i=0; i<3; i++){
		writeI2CMasterByte(ArduCam_I2C_Address, 0x3500+i, regVals[i]);
	}
	
	cam->FrameExposureSettings.shutter= (regVals[0]<<12) + (regVals[1]<<4) + (regVals[2]>>4);
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Set_PCLKValue(Arducam_T *cam, uint8_t val){
	
	uint8_t returnval;
	writeI2CMasterByte(ArduCam_I2C_Address, 0x4837, val);
	HAL_Delay(10);
	readI2CMaster(ArduCam_I2C_Address, 0x4837, 1, &returnval);
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Actual exposure time in milliseconds
 ****************************************************************/
uint32_t ArduCam_Get_ExposureTime(Arducam_T *cam){	//Basamak yuvarlama isini yap, tline value i�in fonksiyon yap
	
	uint8_t regs[5]={0};
	uint16_t tline, regexptime, actualexptime=0;
	uint32_t temp;
	
	HAL_Delay(1000);
	readI2CMaster(ArduCam_I2C_Address, 0x350C, 2, &regs[0]);
	HAL_Delay(10);
	readI2CMaster(ArduCam_I2C_Address, 0x3B04, 2, &regs[2]);
	HAL_Delay(10);
	readI2CMaster(ArduCam_I2C_Address, 0x4837, 1, &regs[4]);

	return actualexptime;
}

/****************************************************************
 * @brief		Desired exposure time in milliseconds
 * @param		Nothing
 * @return	Actual exposure time in milliseconds
 ****************************************************************/
uint32_t ArduCam_Set_ExposureTime(Arducam_T *cam, uint16_t desExp){
	
	uint8_t regs[4];
	uint16_t tline;
	uint32_t tempExp=0;
	
	readI2CMaster(ArduCam_I2C_Address, 0x380C, 2, &regs[0]);
	tline=(regs[0]<<8) | regs[1];
	
	tempExp=desExp*(ArduCam_System_Frequency/1000);
	tempExp/=tline;
//	temp/=1000;
	
	regs[2]=(uint8_t)((tempExp>>8) & 0xFF);
	regs[3]= (uint8_t)(tempExp & 0xFF);
	
	writeI2CMaster(ArduCam_I2C_Address, 0x3B04, &regs[2], 2);
	HAL_Delay(10);
	
	return ArduCam_Get_ExposureTime(cam);
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_Get_ShutterLinePeriod(Arducam_T *cam){
	
	uint8_t tempArr[3]={0x00};
	uint8_t i;
	
	for(i=0; i<2; i++){
		readI2CMasterByte(ArduCam_I2C_Address, (0x350C+i), &tempArr[i]);
		HAL_Delay(10);
	}
	cam->FrameExposureSettings.MaxLine= (tempArr[0]<<8) | tempArr[1];
	
	for(i=0; i<3; i++){
		readI2CMasterByte(ArduCam_I2C_Address, (0x3500+i), &tempArr[i]);
		HAL_Delay(10);
	}
//	cam->FrameExposureSettings.shutter=(tempArr[0]<<16) | (tempArr[1]<<8) | tempArr[2];
	cam->FrameExposureSettings.shutter=(tempArr[0]<<12) + (tempArr[1]<<4) + (tempArr[2]>>4);
}
//---SPI, I2C Register Read/Write Functions--//
/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_RegisterUpdate(const struct sensor_reg reglist[]){
	
	uint16_t reg_addr = 0,  reg_idx = 0;
	uint8_t reg_val = 0;
	const struct sensor_reg *next = reglist;
	uint8_t tmpcnt=0;
	
	while ((reg_addr != 0xffff) | (reg_val != 0xff))
	{
		reg_addr = pgm_read_word(&next->reg);
		reg_val = pgm_read_word(&next->val);

		writeI2CMaster(ArduCam_I2C_Address, reg_addr, &reg_val, 1);
		HAL_Delay(1);
		readI2CMaster(ArduCam_I2C_Address, reg_addr, 1, &tmpcnt);
		if(tmpcnt!=reg_val){
			__nop();
		}
	  next++;
	  reg_idx++;
	}
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_WriteRegister(uint8_t writeAdd, uint8_t writeVal){
	
	writeSPImaster(writeAdd | 0x80, &writeVal, 1);
}

/****************************************************************
 * @brief		
 * @param		Nothing
 * @return	Nothing
 ****************************************************************/
void ArduCam_ReadRegister(uint8_t readAdd, uint8_t *readOut, uint8_t readLength){
	
	readSPImaster(readAdd & 0x7F, readOut, readLength);
}

void writeI2CMaster(uint8_t devAddr, uint16_t writeAddr, uint8_t *data, uint8_t length){

	HAL_I2C_Mem_Write(&hi2c1, (uint16_t) devAddr, writeAddr , 2, data , (uint16_t) length,100);
}

void readI2CMaster(uint8_t devAddr, uint16_t readAddr, uint8_t readLength, uint8_t *readOut){
	
	HAL_I2C_Mem_Read(&hi2c1, (uint16_t) devAddr, readAddr , 2, readOut , (uint16_t) readLength,100);
}

void writeI2CMasterByte(uint8_t devAddr, uint16_t addr, uint8_t data){

	HAL_I2C_Mem_Write(&hi2c1, (uint16_t) devAddr, addr , 2, &data , 1,100);
}


void readI2CMasterByte(uint8_t devAddr, uint16_t addr, uint8_t *data){

	HAL_I2C_Mem_Read(&hi2c1, (uint16_t) devAddr, addr , 2, data , 1,100);
}

void writeSPImaster(uint8_t writeAdd, uint8_t *writeIn, uint8_t writeLength){
	
	uint8_t TXdata[2] = {writeAdd,*writeIn};
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_RESET);
	HAL_SPI_Transmit (&hspi2, TXdata, 2, 10);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_SET);
	
}
void readSPImaster(uint8_t readAdd, uint8_t *readOut, uint8_t readLength){

	uint8_t add = readAdd;
	uint8_t yyy[2] = {readAdd,0};
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_RESET);
	HAL_SPI_Transmit (&hspi2, yyy, 2, 10);
	HAL_SPI_Receive(&hspi2,readOut,(uint16_t) readLength,10);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_SET);
}

void ArduChip_EnterStandby(Arducam_T *cam){

uint8_t temp;

ArduCam_ReadRegister(ArduChip_Address_GPIORW, &temp, 1);
ArduCam_WriteRegister(ArduChip_Address_GPIORW, (temp|ArduChip_GPIO_STDBY_MASK) );
}

void ArduChip_ExitStandby(Arducam_T *cam){

uint8_t temp;

ArduCam_ReadRegister(ArduChip_Address_GPIORW, &temp, 1);

ArduCam_WriteRegister(ArduChip_Address_GPIORW, (temp&(~ArduChip_GPIO_STDBY_MASK)) );
}

/**************************END OF FILE***************************/
