/*
 * SDLibrary.c
 *
 *  Created on: Oct 19, 2019
 *      Author: Eser GUL
 */

/**
 * |----------------------------------------------------------------------
 * | Copyright (C) Eser GUL, 2019
 * |----------------------------------------------------------------------
 */

#include "SDLibrary.h"
#include <string.h>

/* SD Card Object */
SD_Card sd1;

SD_Card_Result SD_Init(SD_Card* DataStruct){
	
		if(f_mount(&(DataStruct->fs), "", 0) != FR_OK) 
			{return SD_Card_Result_MountUnsuccessful;}
		
			
		if(f_getfree("", &(DataStruct->fre_clust), &(DataStruct->pfs)) != FR_OK) 
			{return SD_Card_Result_Error;}
		
		DataStruct->total = (uint32_t)((DataStruct->pfs->n_fatent - 2) * DataStruct->pfs->csize * 0.5); 
		DataStruct->free1 = (uint32_t)(DataStruct->fre_clust * DataStruct->pfs->csize * 0.5); 
		
		if(DataStruct->free1 < 1) 
			{return SD_Card_Result_InsufficientSpace;}
			
		return SD_Card_Result_Ok;
}

SD_Card_Result SD_PrepareFile(SD_Card* DataStruct,File_Type type){

		if(type == TextFile){	
			
			strcpy(DataStruct->filename, "RECORDxx.txt"); 
			
			for (uint8_t i = 0; i < 100; i++) 
			{
				DataStruct->filename[6] = '0' + i/10;
				DataStruct->filename[7] = '0' + i%10;
				if(f_open(&DataStruct->dummy, DataStruct->filename,FA_OPEN_EXISTING)!=FR_OK)
				{break;}
				f_close(&DataStruct->dummy);
			}	
		
		}
		
		else if(type==ImageFile){	
			
			strcpy(DataStruct->filename, "PHOTOxx.jpg"); 
			
			for (uint8_t i = 0; i < 100; i++) 
			{
				DataStruct->filename[5] = '0' + i/10;
				DataStruct->filename[6] = '0' + i%10;
				if(f_open(&DataStruct->dummy, DataStruct->filename,FA_OPEN_EXISTING)!=FR_OK)
				{break;}
				f_close(&DataStruct->dummy);
			}	
		
		}
		
		else{
			
			return SD_Card_Result_Error;
		}
		
		return SD_Card_Result_Ok;
}


SD_Card_Result SD_Write_String(SD_Card* DataStruct, char data[1024]){

			/* Open file to write */ 
				SD_OpenFile(DataStruct);
			
			/* Writing text */ 
				f_puts(data, &(DataStruct->fil)); 
			
			/* Close file */ 
				SD_CloseFile(DataStruct);
		
				return SD_Card_Result_Ok;

}

SD_Card_Result SD_Write_Byte(SD_Card* DataStruct, uint8_t byte){

	f_putc(byte,&(DataStruct->fil));	
	return SD_Card_Result_Ok;
	
}

SD_Card_Result SD_Read(SD_Card* DataStruct){
	
	uint8_t j=0;

	if(SD_OpenFile(DataStruct)!=SD_Card_Result_Ok) return SD_Card_Result_FileOpenError;
	
	while(f_gets(DataStruct->buffer[j], sizeof(DataStruct->buffer[j]), &(DataStruct->fil))) 
		
	{ 
		for(uint8_t k = 0;k<60;k++)
		{ 
			if(DataStruct->buffer[j][k] == '\n')
			{j++;}
		} 
		
	} 
	
	if(SD_CloseFile(DataStruct)!=SD_Card_Result_Ok) return SD_Card_Result_FileCloseError;
	return SD_Card_Result_Ok;

}

SD_Card_Result SD_Deinit(SD_Card* DataStruct){


	if(f_mount(NULL, "", 1) != FR_OK) 
		return SD_Card_Result_MountUnsuccessful;
	
	return SD_Card_Result_Ok;
}

SD_Card_Result SD_OpenFile(SD_Card* DataStruct){

		
	if((f_open(&(DataStruct->fil), DataStruct->filename, FA_OPEN_ALWAYS | FA_READ | FA_WRITE))!=FR_OK)
		return SD_Card_Result_FileOpenError;
	
	return SD_Card_Result_Ok;

}

SD_Card_Result SD_CloseFile(SD_Card* DataStruct){

		
	if(f_close(&(DataStruct->fil))!=FR_OK)
		return SD_Card_Result_FileCloseError;
	
	return SD_Card_Result_Ok;

}
