/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Author: 			Emirhan Eser GUL
	* Organization: USTTL
	* Date:					18.10.2019
  * Interfaces:   I2C1 (ArduCam,EPS,ADCS), I2C3 (GY87), SPI2 (ArduCam), SPI3 (SD Card)
	* Last Update:	20.12.2019 || Eser GUL || Subsystem Reset Commands Added
	*								24.12.2019 || Eser GUL || Photo size transmit added to arducam.c
	*								25.12.2019 || Eser GUL || uint16t converter fixed
  *
  ******************************************************************************
  */

/* Includes */
#include "EDUSAT_SYSTEM.h"

/*Variables for telemetry*/
char telemetry[140];
long timepassed = 0;
uint16_t period = 600;
uint8_t control[4];
/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
	EDUSAT_Init();	
	
  while (1)
		{	
			if (strlen((char*)buff)>5)
				{		
						Blink(1);
						uint8_t temp[5];
								
						if(strncmp((char*)buff,"tlmtry",6)==0)
							{
									AcquireTelemetry(telemetry);
									HAL_UART_Transmit_IT(&huart4,(uint8_t*)telemetry,sizeof(telemetry));	//Sends Telemetry
							}	
						else if (strncmp((char*)buff,"r",1)==0)//r+3200
							{
									for(uint8_t i=0;i<4;i++)
										{
											temp[i] = (uint8_t)buff[i+2];
										}		
									if(SetRW(buff[1],temp)) SendError(ADCS_Error);
									else
										{
											HAL_Delay(5000);
											GetRW(control);
											if(convertToInt16(temp,4)*0.5 < convertToInt16(control,4) || convertToInt16(control,4) < convertToInt16(temp,4)*1.5) SendError(RW_Error);
										}
							}	
						else if (strncmp((char*)buff,"tphoto",5)==0)
							{
								TakePhoto(true);
							}
						else if (strncmp((char*)buff,"openan",6)==0)
							{
								OpenAntennas();
							}
						else if (strncmp((char*)buff,"sreset",6)==0)
							{
								ADCS_Reset();
								EPS_Reset();
								HAL_NVIC_SystemReset();
							}
						else if (strncmp((char*)buff,"p",1)==0)
							{
								for(uint8_t i=0;i<5;i++) 
									{
											temp[i] = buff[i+1];
									}
								period = convertToInt16(temp,5);
							}
						else if (strncmp((char*)buff,"cares",5)==0)
							{
								ArduCam_Set_JPEGSize(&Cam1,buff[5]-48); //0x01, 0x02, 0x03.....0x07 resolution settings
							}
						else if (strncmp((char*)buff,"abdr",4)==0)
							{
								for(uint8_t i=0;i<2;i++)
								{
									temp[i] = (uint8_t)buff[i+4];
								}		
								SetAntennaBurnDuration(convertToInt16(temp,2));
							}
					clearbuffer();	
					Blink(1);			
		}

		Use_MPU(); 
		Use_BMP(); 
		Use_Magneto(); 

		if(HAL_GetTick() - timepassed > period*1000)
		{
			Blink(2);
			AcquireTelemetry(telemetry);
			SaveToSD(telemetry);
			timepassed+=(1000*period);
			Blink(2);
		}
		
  }

}


/************************ (C) COPYRIGHT USTTL *****END OF FILE****/
