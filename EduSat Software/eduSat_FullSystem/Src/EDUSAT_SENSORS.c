/**
  ******************************************************************************
  * @file           : EDUSAT_SENSORS.c
  * @brief          : Sensor functions
  ******************************************************************************
  * @attention
  *
  * Author: 			Emirhan Eser GUL
	* Organization: USTTL
	* Date:					25.12.2019
  * Interfaces:   I2C3 (GY87)
	* Last Update:	20.12.2019 || Eser GUL || Initial transfer from main.c
  *
  ******************************************************************************
  */
	
#include "EDUSAT_SENSORS.h"
#include <math.h>

SensorData GY87 = {0};
char sensors[81];

void Use_BMP() {
	
				GY87.Temp = BMP085calculateTemperature(BMP085readUT(&hi2c3))*0.1;
				GY87.P = BMP085calculatePressure(BMP085readUP(&hi2c3));
				GY87.Altitude = (float)44330 * (1 - pow(((float) GY87.P/101325), 0.190295));
}

void Use_MPU() {
	
		if(SD_MPU6050_Init(&hi2c3,&mpu1,SD_MPU6050_Device_0,SD_MPU6050_Accelerometer_2G,SD_MPU6050_Gyroscope_250s ) == SD_MPU6050_Result_Ok)
	  {
			SD_MPU6050_ReadGyroscope(&hi2c3,&mpu1);
			GY87.g_x = mpu1.Gyroscope_X/131.0+3;
			GY87.g_y = mpu1.Gyroscope_Y/131.0+1.2;
			GY87.g_z = mpu1.Gyroscope_Z/131.0+0.2;
			SD_MPU6050_ReadAccelerometer(&hi2c3,&mpu1);
			GY87.a_x = (mpu1.Accelerometer_X*9.81)/16384+0.4;
			GY87.a_y = (mpu1.Accelerometer_Y*9.81)/16384+0.2;
			GY87.a_z = (mpu1.Accelerometer_Z*9.81)/16384+0.8;
			return;
	  }
		SendError(MPU_Error);

}

void Use_Magneto() {
	
	if(SD_MPU6050_Init(&hi2c3,&mpu1,SD_MPU6050_Device_0,SD_MPU6050_Accelerometer_2G,SD_MPU6050_Gyroscope_250s ) == SD_MPU6050_Result_Ok) 
		SD_MPU6050_setBypass(&hi2c3,&mpu1,1);
	
	ConfigureHMC5883L();
	GY87.Mx = GetX();
	if(GY87.Mx == 0xFFFF) {SendError(Magneto_Error); return;}
	GY87.My = GetY();
	GY87.Mz = GetZ();

}

void getSensorData(){
	sprintf(sensors,"%.2f,%06ld,%f,%f,%f,%f,%f,%f,%d,%d,%d,%.2f", GY87.Temp, GY87.P,GY87.a_x,GY87.a_y,GY87.a_z,GY87.g_x,GY87.g_y,GY87.g_z,GY87.Mx,GY87.My,GY87.Mz,GY87.Altitude);
}

/*********COPYRIGHT (C) USTTL **END OF FILE****/
