#include "ADCS.h"

/*Variable to store the incoming data*/
uint8_t ldr[25] = "0000,0000,0000,0000,0000";

ADCS_Status getLDRdata(){
	 if(HAL_I2C_Master_Receive(&hi2c1,ADCSAddr<<1,ldr,sizeof(ldr),10)!=HAL_OK) return LDR_Error;
	 return ADCS_OK;
}

ADCS_Status SetRW(uint8_t direction,uint8_t speed[4]){
	 uint8_t command[5];
	 command[0] = direction;
	
	 for(uint8_t i=0;i<4;i++){
		 command[i+1] = speed[i];
	 }
	 
   if(HAL_I2C_Master_Transmit(&hi2c1,ADCSAddr<<1,command,sizeof(command),10)!=HAL_OK) return ADCS_Comm_Error;
	 return ADCS_OK;
}

ADCS_Status GetRW(uint8_t* control){
	 if(HAL_I2C_Master_Receive(&hi2c1,ADCSAddr<<1,ldr,sizeof(ldr),10)!=HAL_OK) return LDR_Error;
	 for(uint8_t i=0;i<4;i++){
		 control[i] = ldr[i+20];
	 }	 
	 return ADCS_OK;
}

ADCS_Status ADCS_Reset(){
	
	 uint8_t command[10] = "soft_reset";	 
   if(HAL_I2C_Master_Transmit(&hi2c1,ADCSAddr<<1,command,sizeof(command),10)!=HAL_OK) return ADCS_Comm_Error;
	 return ADCS_OK;
}
