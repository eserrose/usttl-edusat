/**
  ******************************************************************************
  * @file           : EDUSAT_SENSORS.h
  * @brief          : Main sensor header
  ******************************************************************************
  * @attention
  *
  * Author: 			Emirhan Eser GUL
	* Organization: USTTL
	* Date:					25.12.2019
  * Interfaces:   I2C3 (GY87)
	* Last Update:	20.12.2019 || Eser GUL || Initial transfer from main.h
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EDUSATSENSORS_H
#define __EDUSATSENSORS_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "EDUSAT_SYSTEM.h"
#include "bmp085.h"
#include "HMC5883L.h"
#include "sd_hal_mpu6050.h"


typedef struct {
	float g_x;
	float g_y;
	float g_z;
	float a_x;
	float a_y;
	float a_z;
	uint16_t Mx;
	uint16_t My;
	uint16_t Mz;
	float Temp;
	long P;
	float Altitude;
} SensorData;


void Use_BMP(void);
void Use_MPU(void);
void Use_Magneto(void);
void getSensorData(void);

extern SensorData GY87;
extern char sensors[81];

#ifdef __cplusplus
}
#endif

#endif /* __EDUSATSENSORS_H */

/************************ (C) COPYRIGHT USTTL *****END OF FILE****/
