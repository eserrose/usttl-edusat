/**
  ******************************************************************************
  * @file    HMC5883L.h
  * Created on: May 16, 2019
  * Author: Eser Gul
*/

#ifndef __HMC5883L_H__
#define __HMC5883L_H__

#include "stm32f4xx_hal.h"
// adresses     
// HMC5883l - ADDRESS
#define HMC5883l_ADDRESS 0x1E<<1
// HMC5883l - Registers
#define HMC5883L_RA_CONFIG_A        0x00
#define HMC5883L_RA_CONFIG_B        0x01
#define HMC5883L_RA_MODE            0x02
#define HMC5883L_RA_DATAX_H         0x03
#define HMC5883L_RA_DATAX_L         0x04
#define HMC5883L_RA_DATAZ_H         0x05
#define HMC5883L_RA_DATAZ_L         0x06
#define HMC5883L_RA_DATAY_H         0x07
#define HMC5883L_RA_DATAY_L         0x08
#define HMC5883L_RA_STATUS          0x09
#define HMC5883L_RA_ID_A            0x0A
#define HMC5883L_RA_ID_B            0x0B
#define HMC5883L_RA_ID_C            0x0C
// CONTROL REG A
#define HMC5883l_Enable_A 0x70
// CONTROL REG B
#define HMC5883l_Enable_B 0xA0
// MODE REGISTER
#define HMC5883l_MR 0x00
// HMC5883l - MSB / LSB ADDRESSES
#define HMC5883l_ADD_DATAX_MSB (0x03)
#define HMC5883l_ADD_DATAX_LSB (0x04)
#define HMC5883l_ADD_DATAZ_MSB (0x05)
#define HMC5883l_ADD_DATAZ_LSB (0x06)
#define HMC5883l_ADD_DATAY_MSB (0x07)
#define HMC5883l_ADD_DATAY_LSB (0x08)
// SUM (MSB + LSB) DEFINE
#define HMC5883l_ADD_DATAX_MSB_MULTI (HMC5883l_ADD_DATAX_MSB | 0x80)
#define HMC5883l_ADD_DATAY_MSB_MULTI (HMC5883l_ADD_DATAY_MSB | 0x80)
#define HMC5883l_ADD_DATAZ_MSB_MULTI (HMC5883l_ADD_DATAZ_MSB | 0x80) 

void ConfigureHMC5883L(void);
uint16_t GetX(void);
uint16_t GetY(void);
uint16_t GetZ(void);

#endif   // __HMC5883L_H__
