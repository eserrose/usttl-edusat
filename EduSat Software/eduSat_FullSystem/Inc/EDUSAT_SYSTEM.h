/**
  ******************************************************************************
  * @file           : EDUSAT_SYSTEM.h
  * @brief          : Main driver header
  ******************************************************************************
  * @attention
  *
  * Author: 			Emirhan Eser GUL
	* Organization: USTTL
	* Date:					25.12.2019
  * Interfaces:   I2C1 (ArduCam,EPS,ADCS), I2C3 (GY87), SPI2 (ArduCam), SPI3 (SD Card)
	* Last Update:	20.12.2019 || Eser GUL || Initial transfer from main.h
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EDUSATSYSTEM_H
#define __EDUSATSYSTEM_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "string.h"
#include <stdio.h>
#include <time.h>
#include "EDUSAT_SENSORS.h"
#include "Arducam.h"
#include "EPS.h"
#include "ADCS.h"
#include "SDLibrary.h"

/* Defines --------------------------------------------------------------------*/
#define LED1_Pin GPIO_PIN_2
#define LED1_GPIO_Port GPIOE
#define LED2_Pin GPIO_PIN_3
#define LED2_GPIO_Port GPIOE
#define SDCS_Pin GPIO_PIN_1
#define SDCS_GPIO_Port GPIOD
#define CS_Pin GPIO_PIN_3
#define CS_GPIO_Port GPIOD

/*Struct for errors*/
typedef enum  {
	MPU_Error 			= 0x01, 		/*!< Cannot connect to MPU 0000 0001*/ 
	Magneto_Error   = 0x02,  		/*!< SD Card has an error 0000 0010*/
	Barometer_Error	= 0x04,			/*!< Cannot connect to HMC5883l 0000 0100 */
	SD_Card_Error   = 0x08,			/*!< Cannot connect to BMP085 0000 1000 */
	Cam_Error				= 0x10,		  /*!< Camera has an error 0001 0000*/
	RW_Error				= 0x20,			/*!< Wrong RPM 0010 0000*/
	EPS_Error				= 0x40,			/*!< EPS Data not received 0100 0000*/
	ADCS_Error			= 0x80,			/*!< ADCS Data not received 1000 0000*/
	LED_Error				= 0x03	  	/*!< Wrong led error */
} ErrorCode;



void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2C3_Init(void);
static void MX_SPI2_Init(void);
static void MX_SPI3_Init(void);
static void MX_UART4_Init(void);
void Error_Handler(void);
void EDUSAT_Init(void);

void clearbuffer(void);
void TakePhoto(bool);
void SendError(ErrorCode);
void Blink(uint8_t);
void AcquireTelemetry(char*);
void SaveToSD(char*);
uint16_t convertToInt16(uint8_t*,uint8_t);
uint16_t power(uint8_t,uint8_t);

extern I2C_HandleTypeDef hi2c1;
extern I2C_HandleTypeDef hi2c3;
extern SPI_HandleTypeDef hspi2;
extern SPI_HandleTypeDef hspi3;
extern UART_HandleTypeDef huart4;
extern uint8_t buff[7];

#ifdef __cplusplus
}
#endif

#endif /* __EDUSATSYSTEM_H */

/************************ (C) COPYRIGHT USTTL *****END OF FILE****/
