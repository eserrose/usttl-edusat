/****************************************************************
 * EPS Drivers in Embedded C Language
 *
 * Organisation		:	  ITU USTTL
 * Author			    :   Emirhan Eser GUL
 * Date			      :   20.11.2019
 * Target MCU		  :   STM32F407VET6
 * Target Module	:   STM32F1 "Bluepill"
 * Interface(s)		:   I2C
 * Version			  :   v1.2
 *
 ****************************************************************/
 
/****************************************************************
 * Revision History
 *
 * v1.0 || 20.11.2019 || Eser GUL || Initial Release
 * v1.1 || 04.12.2019 || Eser GUL || Error Codes Added
 * v1.2 || 20.12.2019 || Eser GUL || Reset Function Added
 *
 ****************************************************************/
 
#ifndef __EPS_H
#define __EPS_H
 
#include "stm32f4xx_hal.h"
 
#define EPSAddr 0x05 //Defining EPS address
 
extern I2C_HandleTypeDef hi2c1;
extern uint8_t epsdata[26];

typedef enum{
	EPS_OK = 0x00,
	EPS_Comm_Error
}EPS_Status;

EPS_Status getEPSdata(void);
EPS_Status OpenAntennas(void);
EPS_Status SetAntennaBurnDuration(uint8_t);
EPS_Status EPS_Reset(void);
 
#endif

/**************************END OF FILE***************************/
