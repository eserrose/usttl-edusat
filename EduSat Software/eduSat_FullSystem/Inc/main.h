/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "string.h"
#include <stdio.h>
#include <time.h>
#include "bmp085.h"
#include "HMC5883L.h"
#include "sd_hal_mpu6050.h"
#include "Arducam.h"
#include "EPS.h"
#include "ADCS.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
typedef enum  {
	MPU_Error 			= 0x01, 		/*!< Cannot connect to MPU 0000 0001*/ 
	SD_Card_Error 	= 0x02,  		/*!< SD Card has an error 0000 0010*/
	Magneto_Error		= 0x04,			/*!< Cannot connect to HMC5883l 0000 0100 */
	Barometer_Error = 0x08,			/*!< Cannot connect to BMP085 0000 1000 */
	Cam_Error				= 0x10,		  /*!< Camera has an error 0001 0000*/
	RW_Error				= 0x20,			/*!< Wrong RPM 0010 0000*/
	EPS_Error				= 0x40,			/*!< EPS Data not received 0100 0000*/
	ADCS_Error			= 0x80,			/*!< ADCS Data not received 1000 0000*/
	LED_Error				= 0x03	  	/*!< Wrong led error */
} ErrorCode;

typedef struct {
	float g_x;
	float g_y;
	float g_z;
	float a_x;
	float a_y;
	float a_z;
	uint16_t Mx;
	uint16_t My;
	uint16_t Mz;
	float Temp;
	long P;
		
} SensorData;
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void clearbuffer(void);
static void Use_BMP(void);
static void Use_MPU(void);
static void Use_Magneto(void);
static void TakePhoto(void);
void SendError(ErrorCode);
void Blink(uint8_t);
void AcquireTelemetry(char*);
void GetTime(void);
void SaveToSD(char*);
uint16_t convertToInt16(uint8_t*,uint8_t);
uint16_t power(uint8_t,uint8_t);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED1_Pin GPIO_PIN_2
#define LED1_GPIO_Port GPIOE
#define LED2_Pin GPIO_PIN_3
#define LED2_GPIO_Port GPIOE
#define SDCS_Pin GPIO_PIN_1
#define SDCS_GPIO_Port GPIOD
#define CS_Pin GPIO_PIN_3
#define CS_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */


/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
