/****************************************************************
 * ADCS Drivers in Embedded C Language
 *
 * Organisation		:	ITU USTTL
 * Author			:   Emirhan Eser GUL
 * Date			    :   20.11.2019
 * Target MCU		:   STM32F407VET6
 * Target Module	:   STM32F1 "Bluepill"
 * Interface(s)		:   I2C
 * Version			:   v1.1
 *
 ****************************************************************/
 
/****************************************************************
 * Revision History
 *
 * v1.0 || 20.11.2019 || Eser GUL || Initial Release
 * v1.1 || 04.12.2019 || Eser GUL || Error Codes Added
 * v1.2 || 20.12.2019 || Eser GUL || Reset Function Added
 *
 ****************************************************************/
 
#ifndef __ADCS_H
#define __ADCS_H
 
#include "stm32f4xx_hal.h"
 
#define ADCSAddr 0x04 //Defining ADCS address
 
extern I2C_HandleTypeDef hi2c1;
extern uint8_t ldr[25]; 

typedef enum{
	ADCS_OK = 0x00,
	LDR_Error,
	ADCS_Comm_Error
}ADCS_Status;

ADCS_Status getLDRdata(void);
ADCS_Status SetRW(uint8_t,uint8_t*);
ADCS_Status GetRW(uint8_t*);
ADCS_Status ADCS_Reset(void);

#endif

/**************************END OF FILE***************************/
