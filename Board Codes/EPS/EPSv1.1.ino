#include <Wire_slave.h>

//Pins are defined.
#define BATT_VOL_PIN PB0 
#define BATT_CURR_PIN PA7
#define SOL_VOL_PIN PB1
#define SOL_CURR_PIN PA6

#define SOFT_RESET     "soft_reset"
#define ANTENNA_STATUS "antenna_on"
#define ANTENNA_BURNDR "duration"
#define ANTENNA_PIN PA1

//variables for voltage and current of battery
//variables for voltage and current of solar panel
float batt_voltage, batt_current, solar_voltage, solar_current;

//If True(1)then antenna opened 
int antenna_stat = 0;

//For openinng antenna
int duration = 15000;

//variable for collecting all measured value
char data_to_send[25];
String incoming_command = "";

void setup() {

  pinMode(ANTENNA_PIN, OUTPUT);
  I2C1->sda_pin = 9;
  I2C1->scl_pin = 8;
  Wire.begin(5);

  Wire.onReceive(taking_data);
  Wire.onRequest(sending_data);
}

void loop() {

  sampling_sensors();

  if(incoming_command == ANTENNA_STATUS)
  {
    opening_antenna();
    incoming_command = "";
  }
  else if (incoming_command == SOFT_RESET)
  {
    nvic_sys_reset();
  }
  else if (incoming_command.substring(0,8) == ANTENNA_BURNDR)
  {
    duration = incoming_command.substring(9).toInt();
  }

}

void sampling_sensors()
{
  batt_voltage = analogRead(BATT_VOL_PIN); // read the value from the sensor
  batt_current = analogRead(BATT_CURR_PIN); // read the value from the sensor
  batt_voltage = 5*3.3*batt_voltage/4095; // 5 volt correspond to 1 volt
  batt_current = 3.3*batt_current/4095;// 1 volt/amp

  solar_voltage = analogRead(SOL_VOL_PIN); // read the value from the sensor
  solar_current = analogRead(SOL_CURR_PIN); // read the value from the sensor
  solar_voltage = 5*3.3*solar_voltage/4095; // 5 volt correspond to 1 volt
  solar_current = 3.3*solar_current/4095;// 1 volt/amp

  sprintf(data_to_send,"%05.2f,%05.2f,%05.2f,%05.2f,%d",batt_voltage,batt_current,solar_voltage,solar_current,antenna_stat);

}
void opening_antenna()
{
  digitalWrite(ANTENNA_PIN, HIGH);
  delay(duration);
  digitalWrite(ANTENNA_PIN, LOW);
  antenna_stat = 1;
}


void taking_data(int howMany)
{
  incoming_command = "";
  while(Wire.available()){
    incoming_command += (char)Wire.read();   
  }
}
void sending_data()
{
  Wire.write(data_to_send);
}
