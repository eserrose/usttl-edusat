#ifndef __BUZZER_H
#define __BUZZER_H

#include "Arduino.h"
#define BUZZER_PIN PB15

  //Buzzer settings

void mors(char);
void play_morse(String);
void MorseOff(int);
void MorseDash();
void MorseDot();

#endif /* __BUZZER_H */