**Programming the Subsystems**

1.  Download the Arduino IDE from the [official link](https://www.arduino.cc/en/main/software)
2.  Under Arduino, click File/Preferences; then click on the button on the right of “Additional Boards Manager URLs”
3.  Add the following address into the textbox then click OK:
       http://dan.drown.org/stm32duino/package_STM32duino_index.json
4. Click on Tools/Board/Board Manager. Under Type; select “Contributed” and install the latest version of “STM32 Core by STMicroelectronics”
5. Go to "Tools" Tab and do the following selections:
*     Board: Generic STM32F103C Series
*     Variant: STM32F103C8 (20k RAM, 64k Flash)
*     Upload Method: "STM32duino bootloader"
*     CPU Speed(MHz): "72Mhz (Normal)"
*     Optimize: "Smallest (default)"

6. Open the desired subsystem code and make the desired edits.
7. Connect the STM32F1 Board on the subsystem via a micro usb cable, select its Port under "Tools" and click "Upload"

