/**
  ******************************************************************************
  * @file           : ADCS.ino
  * @brief          : ADCS Subsystem Code
  ******************************************************************************
  * @attention
  *
  * Author: 	    Aykut UCTEPE & Emirhan Eser GUL
  * Organization: USTTL
  * Date:		      18.10.2019
  * Version:      1.3 || 20.12.2019 || Aykut UCTEPE || Comments are added
  * Interfaces:   I2C (Slave)
  *
  ******************************************************************************
  */

#include <Wire_slave.h> //Slave library added for STM32F103C
#define IN1     PB11 //Input pin1 for motordriver is defined.
#define IN2     PB10 //Input pin2 for motordriver is defined.
#define PWMEN   PA1  //EN pin for motordriver is defined.
#define HALLPIN PB15 //Hall effect sensors input pin is defined.

String speedy = "";
int pwm_Signal, temp;
int counter = 0, rpm = 0, passedtime = 0;
char sign,vals[5][5] = {{"0000"}};
char Telemetry[24];

void setup() {
    pinMode(IN1, OUTPUT); //Declaring the pin modes, obviously they're outputs
    pinMode(IN2, OUTPUT);
    //IN1 and IN2 pins set high in order to block rotation.
    digitalWrite(IN1,HIGH); 
    digitalWrite(IN2,HIGH);

    Wire.begin(4); //Join the I2C Bus with address 0x04
    Wire.onRequest(istekGeldiginde); //When Master reads
    Wire.onReceive(veriGeldiginde);  //When Master writes

    pinMode(HALLPIN,INPUT);
    attachInterrupt(HALLPIN, rpmpulse, FALLING);   // Interrupt is called during the falling edge
}

void loop() {
      readLDR(); //Data from LDR sensors are taken.
} 

void istekGeldiginde(){
    calculateRPM();         //rpm calculator function is called.
    sprintf(Telemetry, "%s,%s,%s,%s,%s",vals[0],vals[1],vals[2],vals[3],vals[4]); 
    Wire.write(Telemetry);
}

void veriGeldiginde(int x){
    while(Wire.available()) speedy += (char)Wire.read();
    if(speedy == "soft_reset") nvic_sys_reset();
    temp = speedy.substring(1,5).toInt();
    sign = speedy.charAt(0);
    pwm_Signal = map(temp, 0, 3400, 0, 255);
    if (sign == '-') Rotate(pwm_Signal*-1,false);
    else Rotate(pwm_Signal,true);
    speedy = "";
   
}

void rpmpulse(){
    counter++; //Update count
}

void calculateRPM(){
    detachInterrupt(HALLPIN);                                   //Disable interrupts during update
    rpm = (60 * counter) / ((millis() - passedtime) / 1000);    //Calculations required for RPM
    counter = 0;
    sprintf(vals[4],"%04d", rpm);
    attachInterrupt(HALLPIN, rpmpulse, FALLING);                   //Re-enable Interrupts
}

void readLDR(){
    sprintf(vals[0],"%04d", analogRead(PA6)); //LDR data is read from the sensors.
    sprintf(vals[1],"%04d", analogRead(PA5));
    sprintf(vals[2],"%04d", analogRead(PA4));
    sprintf(vals[3],"%04d", analogRead(PA3));
}

void Rotate(int rpm,bool reverse){
  
  if(reverse){
   digitalWrite(IN1,LOW); //By setting one of the in pins high and other low, dc motor starts rotating.
   digitalWrite(IN2,HIGH);
    }
  else{
   digitalWrite(IN1,HIGH); //Motor starts rotating awkward rotation
   digitalWrite(IN2,LOW);
  }   
  analogWrite(PWMEN,rpm);
  passedtime = millis();    
 }


  
